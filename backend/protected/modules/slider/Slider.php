<?php
namespace backend\modules\slider;

use yii\base\Module;

/**
 * Backend-модуль [[Slider]]
 */
class Slider extends \common\modules\slider\Slider
{
	public $controllerNamespace = 'backend\modules\slider\controllers';
}