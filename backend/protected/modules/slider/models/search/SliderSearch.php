<?php
namespace backend\modules\slider\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\slider\models\Slider;

/**
 * Класс для поиском по модели User
 *
 * @property string $q
 * @property integer $type
 * @property integer $create_time
 */
class SliderSearch extends Model
{
	/**
	 * @var string Заголовок.
	 */
	public $title;

	/**
	 * @var integer Статус.
	 */
	public $status_id;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			// Заголовок [[title]]
			['title', 'string', 'max' => 255]
		];
	}

	/**
	 * Поиск по переданным критериям пользователя
	 * @param array|null массив с критериями для выборки
	 * @return yii\data\ActiveDataProvider dataProvider с результатами поиска
	 */
	public function search($params)
	{
		$query = Slider::find()->orderBy('ordering ASC');
		$dataProvider = new ActiveDataProvider([
			'query' => $query
		]);
		
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

        $this->addCondition($query, 'title', true);

		return $dataProvider;
	}

	/**
	 * Функция добавления условий поиска.
	 * @param yii\db\Query $query Экземпляр выборки.
	 * @param string $attribute Имя отрибута по которому нужно искать.
	 * @param boolean $partialMatch Тип добавляемого сравнения. Строгое совпадение или частичное.
	 */
	protected function addCondition($query, $attribute, $partialMatch = false) 
    { 
        $value = $this->$attribute; 
        if (trim($value) === '') { 
            return; 
        } 
        if ($partialMatch) { 
            $query->andWhere(['like', $attribute, $value]); 
        } else { 
            $query->andWhere([$attribute => $value]); 
        } 
    }
}
