<?php
/**
 * @var yii\web\View $this
 * @var backend\modules\users\models\User $model
 * @var yii\widgets\ActiveForm $form
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\extensions\fileapi\FileAPI;
use common\extensions\fileapi\FileAPIAdvanced;

$form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => true,
	'validateOnChange' => false
]); ?>
  <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
                'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
            ]); ?>
        </div>
    </div>

<?= $form->field($model, 'title') .
    $form->field($model, 'content')->textarea(['rows' => 15]) .
     $form->field($model, 'ordering') .
     $form->field($model, 'position_id')->dropDownList($positionArray, ['prompt' => 'Выберите позицию']) .
     $form->field($model, 'status_id')->dropDownList($statusArray, ['prompt' => 'Выберите статус']) .
     $form->field($imageModel, 'name')->widget(FileAPI::className(), [
        'settings' => [
            'url' => Url::toRoute('/slider/images/upload-temp'),
            'multiple' => true
        ]
     ]);

     if (!$model->isNewRecord && $model->images) {
          echo Html::tag('h3', $model->getAttributeLabel('items'));
          echo '<div class="gallery-form-image row">';
          foreach ($model->images as $key => $image) {
               echo '<div class="col-sm-2">' .
                            '<div class="gallery-form-image-delete gallery-form-image-delete-link" data-url="' . Url::toRoute(['/slider/images/delete', 'id' => $image['id']]) . '">' .
                                '<span class="glyphicon glyphicon-trash"></span>' .
                            '</div>' .
                            Html::img($image->url, ['alt' => $image['name'], 'class' => 'img-responsive']) .
                        '</div>';
          }
          echo '</div>';
          echo '<div class="clearfix"></div>';
          $this->registerJs("jQuery(document).on('click', '.gallery-form-image-delete-link', function(evt) {
               evt.preventDefault();
               var image = jQuery(this).parent();
               jQuery.ajax({
                    url : jQuery(this).data('url'),
                    type : 'POST',
                    success : function(data,  textStatus, xhr) {
                         image.remove();
                    }
               });
          });");
    }

     echo Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
     	'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
     ]);
ActiveForm::end();