<?php
/**
 * Страница всех пользователей
 * @var yii\base\View $this
 * @var backend\modules\users\models\User $dataProvider
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\SerialColumn;
use yii\widgets\Menu;
use backend\modules\users\models\User;
use common\modules\users\models\search\UserSearch;
use backend\modules\admin\grid\OrderingColumn;

$this->title = 'Основной слайдер';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'gridId' => 'slider-grid'
];

echo GridView::widget([
    'id' => 'slider-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        [
            'class' => OrderingColumn::className(),
            'attribute' => 'ordering'
        ],
        [
            'attribute' => 'title',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a($model['title'], ['view', 'id' => $model['id']]);
            },
        ],
        [
            'attribute' => 'status_id',
            'value' => function ($model) {
                return $model->status;
            },
            'filter' => Html::activeDropDownList($searchModel, 'status_id', $statusArray, ['class' => 'form-control', 'prompt' => 'Статус'])
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Управление'
        ]
    ]
]);