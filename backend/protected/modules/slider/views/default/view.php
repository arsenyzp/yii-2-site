<?php
/**
 * Страница пользователя
 * @var yii\base\View $this
 * @var backend\modules\users\models\User $model
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model['title'];
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'modelId' => $model['id']
];

// Определяем изображение.
$images = '';
if ($model->images !== null) {
	foreach ($model->images as $image) {
		$images .= Html::img($image->url, ['class' => 'img-responsive']);
	}
}

echo DetailView::widget([
	'model' => $model,
	'attributes' => [
	    'id',
	    'title',
	    'content:html',
	    [
            'attribute' => 'position_id',
            'value' => $model->position
        ],
	    'ordering',
	    [
            'attribute' => 'status_id',
            'value' => $model->status
        ],
	    [
	    	'attribute' => 'items',
	    	'format' => 'html',
	    	'value' => $images
	    ]
	]
]);