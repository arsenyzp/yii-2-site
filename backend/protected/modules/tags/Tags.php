<?php
namespace backend\modules\tags;

/**
 * Backend-модуль [[Tags]]
 */
class Tags extends \common\modules\tags\Tags
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'backend\modules\tags\controllers';
}