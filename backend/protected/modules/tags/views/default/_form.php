<?php
/**
 * Представление формы тэга.
 * @var yii\web\View $this Представление
 * @var yii\widgets\ActiveForm $form Форма
 * @var common\modules\blogs\models\Post $model Модель
 */

use backend\modules\seo\widgets\seo\Seo;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => true,
	'validateOnChange' => false
]); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'name') .
                $form->field($model, 'ordering'); ?>
            <?= Seo::widget([
                'form' => $form,
                'model' => $model,
                'shortForm' => true
            ]); ?>
            <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
                'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
            ]); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
