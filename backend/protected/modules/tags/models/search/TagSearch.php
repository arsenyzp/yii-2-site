<?php
namespace backend\modules\tags\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\tags\models\Tag;

/**
 * Модели поиска по [[Tag]] записям.
 *
 * @property string $name Имя тэга
 */
class TagSearch extends Model
{
	/**
	 * @var string Имя тэга.
	 */
	public $name;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
		    // Имя тэга [[name]]
			['name', 'string'],
		];
	}

	/**
	 * Поиск записей по переданным критериям.
	 * @param array|null Массив с критериями для выборки.
	 * @return yii\data\ActiveDataProvider dataProvider с результатами поиска.
	 */
	public function search($params)
	{
		$query = Tag::find()->orderBy('ordering ASC');
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
			    'pageSize' => Yii::$app->getModule('tags')->recordsPerPage
			]
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'name', true);

		return $dataProvider;
	}

	/**
	 * Функция добавления условий поиска.
	 * @param yii\db\Query $query Экземпляр выборки.
	 * @param string $attribute Имя отрибута по которому нужно искать.
	 * @param boolean $partialMatch Тип добавляемого сравнения. Строгое совпадение или частичное.
	 */
	protected function addCondition($query, $attribute, $partialMatch = false) 
    { 
        $value = $this->$attribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
