<?php
namespace backend\modules\materials;

/**
 * Backend-модуль [[Pages]]
 * Осуществляет всю работу с страницами на backend стороне
 */
class Materials extends \common\modules\materials\Materials
{
	public $controllerNamespace = 'backend\modules\materials\controllers';
}