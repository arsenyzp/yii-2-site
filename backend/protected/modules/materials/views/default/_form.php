<?php
/**
 * Представление формы поста.
 * @var yii\web\View $this Представление
 * @var yii\widgets\ActiveForm $form Форма
 * @var common\modules\blogs\models\Post $model Модель
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\extensions\imperavi\Imperavi;
use common\extensions\fileapi\FileAPIAdvanced;
use backend\modules\tags\widgets\tags\Tags;

$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'validateOnChange' => false
]); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
                'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'title') .
                $form->field($model, 'status_id')->dropDownList($statusArray, [
                    'prompt' => 'Выберите статус'
                ]) .
                $form->field($model, 'parent_id')->dropDownList($parentsArray, [
                    'prompt' => 'Выберите родителя'
                ]) .
                $form->field($model, 'color') .
                $form->field($model, 'ordering'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'tagArray')->widget(Tags::className(), [
                'settings' => [
                    'width' => '100%'
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'image_url')->widget(FileAPIAdvanced::className(), [
                'url' => $this->context->module->imageUrl(),
                'deleteUrl' => Url::toRoute('/blogs/default/delete-image'),
                'deleteTempUrl' => Url::toRoute('/blogs/default/delete-temp-image'),
                'settings' => [
                    'url' => Url::toRoute('upload-temp-image')
                ]
             ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'content')->widget(Imperavi::className(), [
                    'settings' => [
                        'imageUpload' => Url::toRoute('/pages/default/imperavi-upload'),
                        'imageGetJson' => Url::toRoute('/pages/default/imperavi-get'),
                    ]
                ]) .
                Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
                    'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
                ]); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>