<?php

/**
 * Страница всех постов
 * @var yii\base\View $this
 * @var backend\modules\blogs\models\Post $dataProvider
 * @var backend\modules\blogs\models\search\PostSearch $searchModel
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\SerialColumn;
use backend\modules\admin\grid\OrderingColumn;

$this->title = 'Материалы';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'gridId' => 'materials-grid'
];

echo GridView::widget([
    'id' => 'materials-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        [
            'class' => OrderingColumn::className(),
            'attribute' => 'ordering'
        ],
        [
            'attribute' => 'title',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a($model['title'], ['view', 'id' => $model['id']]);
            },
        ],
        [
            'attribute' => 'parent_id',
            'format' => 'html',
            'value' => function ($model) {
                $output = ($model->parent_id !== 0) ? Html::a($model->parent['title'], ['view', 'id' => $model->parent['id']]) : null;
                return $output;
            },
        ],
        [
            'attribute' => 'status_id',
            'value' => function ($model) {
                return $model->status;
            },
            'filter' => Html::activeDropDownList($searchModel, 'status_id', $statusArray, ['class' => 'form-control', 'prompt' => 'Статус'])
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Управление'
        ]
    ]
]);