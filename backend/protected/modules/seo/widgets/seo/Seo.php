<?php
namespace backend\modules\seo\widgets\seo;

use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;

/**
 * Виджет [[Seo]]
 * @var yii\base\Widget $this Виджет
 *
 * Пример использования:
 * ~~~
 * echo Seo::widget([
 *     'form' => $form,
 *     'model' => $model
 * ]);
 * ~~~
 */
class Seo extends Widget
{
    /**
     * Форма виджета
     * @var \yii\db\ActiveRecord
     */
    public $form;

    /**
     * Модель виджета
     * @var \yii\db\ActiveRecord
     */
    public $model;

    public $model_id;

    public $shortForm = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->hasModel() || $this->form === null) {
            throw new InvalidConfigException("Either 'model' and 'form' properties must be specified.");
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $model = ($this->model->isNewRecord || $this->model->seo === null) ? new \common\modules\seo\models\Seo() : $this->model->seo;

        $output = '';
		
		//print_r($this->model);

        if ( !$this->shortForm ) {
            $output .= $this->form->field($model, 'seo_title')->textInput(['maxlength' => 70]);
        }

        $output .= $this->form->field($model, 'meta_description')->textInput(['maxlength' => 156])
            . $this->form->field($model, 'meta_keywords')->textInput();

        if ( !$this->shortForm ) {
            $output .= $this->form->field($model, 'layout')->dropDownList($model::getLayoutArray())
                . $this->form->field($model, 'color')->dropDownList($model::getColorArray())
                . $this->form->field($model, 'page_class')->textInput(['maxlength' => 50])
                . $this->form->field($model, 'css')->textInput(['maxlength' => 255])
                . $this->form->field($model, 'js')->textInput(['maxlength' => 255]);
        }
        return $output;
    }

    /**
     * @return boolean whether this widget is associated with a data model.
     */
    protected function hasModel()
    {
        return $this->model instanceof Model;
    }
}
