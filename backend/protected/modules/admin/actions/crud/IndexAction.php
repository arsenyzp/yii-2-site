<?php
namespace backend\modules\admin\actions\crud;

use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;

/**
 * IndexAction экшен вывода всех записей модели.
 * 
 * Пример использования:
 * ~~~
 * public function actions()
 * {
 *     'index' => [
 *         'class' => IndexAction::className(),
 *         'model' => Post::className(),
 *         'searchModel' => PostSearch::className(),
 *         'view' => 'index',
 *         'params' => [
 *             'statusArray' => Post::getStatusArray()
 *         ]
 *     ]
 * }
 * ~~~
 */
class IndexAction extends Action
{
	/**
	 * @var string Условие сортировки запроса.
	 */
	public $orderBy;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		if ($this->view === null) {
			throw new InvalidConfigException('{$this->view} is required.');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$model = $this->model;

		if ($this->searchModel) {
			$searchModel = new $this->searchModel;
			$dataProvider = $searchModel->search(Yii::$app->getRequest()->getQueryParams());
			$this->params['searchModel'] = $searchModel;
		} else {
			$query = $model::find();
			if ($this->orderBy !== null) {
				$query->orderBy($this->orderBy);
			}
			$dataProvider = new ActiveDataProvider([
				'query' => $query
			]);
		}
		$this->params['dataProvider'] = $dataProvider;

		return $this->controller->render($this->view, $this->params);
	}
}