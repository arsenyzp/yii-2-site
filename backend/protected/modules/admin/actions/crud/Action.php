<?php
namespace backend\modules\admin\actions\crud;

use yii\base\InvalidConfigException;
use yii\web\HttpException;

/**
 * Action общий класс CRUD экшенов.
 */
class Action extends \yii\base\Action
{
	/**
	 * @var string Модель.
	 */
	public $model;

	/**
	 * @var string Поисковая модель.
	 */
	public $searchModel;

	/**
	 * @var string Сценарий модели.
	 */
	public $scenario;

	/**
	 * @var string Представление.
	 */
	public $view;

	/**
	 * @var array Массив параметров представления.
	 */
	public $params = [];

    public $seo = false;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		if ($this->model === null) {
			throw new InvalidConfigException('{$this->model} is required.');
		}
	}

	/**
	 * Поиск модели по первичному ключу.
	 * Если модель не найдена, будет вызвана 404 ошибка.
	 * @param integer|array $id Идентификатор модели.
	 * @return ActiveRecord Найденая модель.
	 * @throws HttpException с кодом 404, если модель не найдена.
	 */
	protected function findModel($id)
	{
		$ar = $this->model;

		if (is_array($id)) {
			$model = $ar::findMultipleIdentity($id);
		} else {
			$model = $ar::findIdentity($id);
		}

		if ($model !== null) {
			return $model;
		} else {
			throw new HttpException(404);
		}
	}
}
