<?php
namespace backend\modules\admin\actions\grid;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\HttpException;

/**
 * Action общий класс CRUD экшенов.
 */
class OrderingAction extends Action
{
	/**
	 * @var string Модель.
	 */
	public $model;

	/**
	 * @var string Сценарий модели.
	 */
	public $scenario;

	/**
	 * @var string Представление.
	 */
	public $view;

	/**
	 * @var array Массив параметров представления.
	 */
	public $params = [];

	/** @var string Имя передаваемого параметра */
	public $name = 'ordering';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		if ($this->model === null) {
			throw new InvalidConfigException('The "model" property must be set.');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		if (($post = Yii::$app->getRequest()->post($this->name)) !== null) {
			$models = $this->findModel(array_keys($post));
			if ($models !== null) {
				foreach ($models as $model) {
					if ($model['ordering'] !== $post[$model['id']]) {
						$model->ordering = $post[$model['id']];
						$model->update(false);
					}
				}
			}
		}
	}

	/**
	 * Поиск модели по первичному ключу.
	 * Если модель не найдена, будет вызвана 404 ошибка.
	 * @param integer|array $id Идентификатор модели.
	 * @return ActiveRecord Найденая модель.
	 * @throws HttpException с кодом 404, если модель не найдена.
	 */
	protected function findModel($id)
	{
		$ar = $this->model;

		if (is_array($id)) {
			$model = $ar::findMultipleIdentity($id);
		} else {
			$model = $ar::findIdentity($id);
		}

		if ($model !== null) {
			return $model;
		} else {
			throw new HttpException(404);
		}
	}
}