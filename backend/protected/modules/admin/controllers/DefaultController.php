<?php
namespace backend\modules\admin\controllers;

use Yii;
use backend\modules\admin\components\Controller;
use common\modules\users\models\User;

/**
 * Основной контроллер модуля Admin
 */
class DefaultController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['access']['rules'][] = [
			'allow' => true,
			'roles' => [User::ROLE_MODERATOR]
		];
		return $behaviors;
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			]
		];
	}

	/**
	 * Главная страница админ-панели.
	 */
	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionTest()
	{
		Yii::$app->mail
    		         ->compose('site/feedback-admin', [
                        'surname' => 'surname',
                        'name' => 'name',
                        'email' => 'test@email.ru',
                        'adminEmail' => 'admin@email.ru',
    		         	'phone' => '112233',
    		         	'body' => 'text'
    		         ])
    		         ->setFrom(['test@email.ru' => 'Name Surname'])
    		         ->setTo('bazillio07@yandex.ru')
    		         ->setSubject('Вопрос на сайте ES')
    		         ->send();
	}
}