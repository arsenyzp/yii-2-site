<?php
namespace backend\modules\admin;

use Yii;
use yii\base\Module;

/**
 * Модуль Admin
 * Данный модуль является основным в приложения
 * Его можно считать как ядро бэкенда
 */
class Admin extends Module
{
	public $controllerNamespace = 'backend\modules\admin\controllers';
}