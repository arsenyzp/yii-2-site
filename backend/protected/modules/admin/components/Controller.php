<?php
namespace backend\modules\admin\components;

use yii\filters\AccessControl;
use common\modules\users\models\User;

/**
 * Основной контроллер backend-приложения.
 * От данного контроллера унаследуются все остальные контроллеры backend-приложения.
 */
class Controller extends \yii\web\Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
				    // Разрешаем доступ нужным пользователям.
					[
						'allow' => true,
						'roles' => [User::ROLE_SUPERADMIN, User::ROLE_ADMIN]
					]
				]
			]
		];
	}

	/**
	 * Поиск модели по первичному ключу.
	 * Если модель не найдена, будет вызвана 404 ошибка.
	 * @param integer|array $id Идентификатор модели.
	 * @return ActiveRecord Найденая модель.
	 * @throws HttpException с кодом 404, если модель не найдена.
	 */
	protected function findModel($id)
	{
		$ar = $this->modelClass;

		if (is_array($id)) {
			$model = $ar::findMultipleIdentity($id);
		} else {
			$model = $ar::findIdentity($id);
		}

		if ($model !== null) {
			return $model;
		} else {
			throw new HttpException(404);
		}
	}
}