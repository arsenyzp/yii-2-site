<?php
/**
 * Представление формы пользователя.
 * @var yii\web\View $this Представление
 * @var yii\widgets\ActiveForm $form Форма
 * @var common\modules\users\models\User $model Модель
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => true,
	'validateOnChange' => false
]);
echo $form->field($model, 'username') .
	 $form->field($model, 'email') .
     $form->field($model, 'password')->passwordInput() .
     $form->field($model, 'repassword')->passwordInput() .
     $form->field($model, 'status_id')->dropDownList($statusArray, [
        'prompt' => 'Выберите статус'
     ]) .
     $form->field($model, 'role_id')->dropDownList($roleArray, [
        'prompt' => 'Выберите роль'
     ]) .
     Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
     	'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
     ]);
ActiveForm::end();