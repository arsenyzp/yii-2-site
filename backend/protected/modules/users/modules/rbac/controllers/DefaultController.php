<?php
namespace backend\modules\users\modules\rbac\controllers;

use Yii;
use yii\web\AccessControl;
use backend\modules\admin\components\Controller;
use common\modules\users\models\User;

/**
 * Основной контроллер backend-модуля [[RBAC]]
 */
class DefaultController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
				    // Разрешаем доступ нужным пользователям
					[
						'allow' => true,
						'actions' => ['create'],
						'roles' => [User::ROLE_SUPERADMIN]
					]
				]
			]
		];
	}

	/**
	 * Создаём иерархию авторизации
	 */
	public function actionCreate()
	{
		$auth = Yii::$app->getAuthManager();
		$auth->clearAll();

		// Роли
		$bizRule = 'return Yii::$app->user->isGuest;';
		$role = $auth->createRole('guest', 'Гость', $bizRule);

		$bizRule = 'return !Yii::$app->user->isGuest;';
		$role = $auth->createRole(User::ROLE_USER, 'Пользователь', $bizRule);
		$role->addChild('guest');

		$role = $auth->createRole(User::ROLE_MODERATOR, 'Модератор');
		$role->addChild(User::ROLE_USER);

		$role = $auth->createRole(User::ROLE_ADMIN, 'Администратор');
		$role->addChild(User::ROLE_MODERATOR);

		$role = $auth->createRole(User::ROLE_SUPERADMIN, 'Супер-администратор');
		$role->addChild(User::ROLE_ADMIN);

		$auth->save();

		$this->redirect(['/users/default/index']);
	}
}