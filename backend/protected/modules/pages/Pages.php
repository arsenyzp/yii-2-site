<?php
namespace backend\modules\pages;

/**
 * Backend-модуль [[Pages]]
 * Осуществляет всю работу с страницами на backend стороне
 */
class Pages extends \common\modules\pages\Pages
{
	public $controllerNamespace = 'backend\modules\pages\controllers';
}