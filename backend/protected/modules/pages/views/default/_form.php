<?php
/**
 * @var yii\web\View $this
 * @var backend\modules\users\models\User $model
 * @var yii\widgets\ActiveForm $form
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use common\extensions\imperavi\Imperavi;
use common\extensions\fileapi\FileAPIAdvanced;
use backend\modules\tags\widgets\tags\Tags;
use backend\modules\seo\widgets\seo\Seo;

$form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => true,
	'validateOnChange' => false
]); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
                'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= Html::tag('h3', 'Информация поста') .
                $form->field($model, 'title') .
                $form->field($model, 'alias') .
                $form->field($model, 'status_id')->dropDownList($statusArray, ['prompt' => 'Выберите статус']) .
                $form->field($model, 'parent_id')->dropDownList($parentsArray, ['prompt' => 'Выберите родительскую страницу']) .
                $form->field($model, 'service')->checkbox() .
                $form->field($model, 'info')->checkbox() .
                $form->field($model, 'image_url')->widget(FileAPIAdvanced::className(), [
                    'url' => $this->context->module->imageUrl(),
                    'deleteUrl' => Url::toRoute('/pages/default/delete-image'),
                    'deleteTempUrl' => Url::toRoute('/pages/default/delete-temp-image'),
                    'settings' => [
                        'url' => Url::toRoute('/pages/default/upload-temp-image'),
                        'imageTransform' => [
                            'imageOriginal' => false,
                            'width' => $this->context->module->imageWidth,
                            'height' => $this->context->module->imageHeight,
                            'preview' => true
                        ]
                    ]
                 ]);
            ?>
        </div>
        <div class="col-sm-6">
            <?= Html::tag('h3', 'SEO, CSS и JS') .
                Seo::widget([
                    'form' => $form,
                    'model' => $model
                ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'tagArray')->widget(Tags::className(), [
                'settings' => [
                    'width' => '100%'
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'content')->widget(Imperavi::className(), [
                'settings' => [
                    'imageUpload' => Url::toRoute('/pages/default/imperavi-upload'),
                    'imageGetJson' => Url::toRoute('/pages/default/imperavi-get'),
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
                'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
            ]); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php echo Yii::$app->controller->renderPartial('/default/gallery/form', ['pageAlias' => $model->alias]); ?>