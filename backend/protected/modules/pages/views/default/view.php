<?php
/**
 * Страница категории
 * @var yii\base\View $this
 * @var backend\modules\blogs\modules\category\models\Category $model
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Menu;

$this->title = $model['title'];
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'modelId' => $model['id']
];

echo DetailView::widget([
	'model' => $model,
	'attributes' => [
	    'id',
	    'title:html',
        'alias',
        [
            'attribute' => 'parent_id',
            'value' => isset($model->parent) ? $model->parent['title'] : ''
        ],
	    [
            'attribute' => 'status_id',
            'value' => $model->status
        ],
        'content:html'
	]
]);