<?php
/**
 * @var $addedImages array
 */
?>

<div class="panel panel panel-default">
    <div class="panel-heading">Код галлереи</div>
    <div class="panel-body">
        <textarea style="width: 100%; height: 220px;">
            <?php echo htmlspecialchars(Yii::$app->controller->renderPartial('/default/gallery/_galleryCode', ['addedImages' => $addedImages])); ?>
        </textarea>
    </div>
</div>