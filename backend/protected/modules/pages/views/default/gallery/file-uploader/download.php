<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <input type="text" name="gallery-image-title[]" placeholder="Title"/>
        </td>
        <td>
            <input type="text" name="gallery-image-alt[]" placeholder="Alt"/>

            <input type="hidden" name="gallery-image-big[]" value="{%=file.bigUrl%}"/>
            <input type="hidden" name="gallery-image-middle[]" value="{%=file.middleUrl%}"/>
            <input type="hidden" name="gallery-image-small[]" value="{%=file.smallUrl%}"/>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Удалить</span>
                </button>

            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Отмена</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}

</script>

<!--<p>
    <input type="text" name="gallery-image-title[]" placeholder="Title"/><br>
    <input type="text" name="gallery-image-alt[]" placeholder="Alt"/>
    <input type="hidden" name="gallery-image-big[]" value="{%=file.bigUrl%}"/>
    <input type="hidden" name="gallery-image-middle[]" value="{%=file.middleUrl%}"/>
    <input type="hidden" name="gallery-image-small[]" value="{%=file.smallUrl%}"/>
</p>-->