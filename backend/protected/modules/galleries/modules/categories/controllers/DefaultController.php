<?php
namespace backend\modules\galleries\modules\categories\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\web\HttpException;
use yii\widgets\ActiveForm;
use common\modules\galleries\modules\categories\models\Category;
use backend\modules\admin\components\Controller;
use backend\modules\admin\actions\crud\IndexAction;
use backend\modules\admin\actions\crud\ViewAction;
use backend\modules\admin\actions\crud\CreateAction;
use backend\modules\admin\actions\crud\UpdateAction;
use backend\modules\admin\actions\crud\DeleteAction;
use backend\modules\admin\actions\crud\BatchDeleteAction;
use backend\modules\admin\actions\grid\OrderingAction;

/**
 * Основной контроллер модуля [[Categories]]
 */
class DefaultController extends Controller
{
	/**
	 * @return string Класс основной модели контролера.
	 */
	public function getModelClass()
	{
		return Category::className();
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
		    'index' => [
		    	'class' => IndexAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'index',
		    	'params' => [
		    		'statusArray' => Category::getStatusArray()
		    	]
		    ],
		    'view' => [
		    	'class' => ViewAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'view'
		    ],
		    'create' => [
		    	'class' => CreateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'create',
		    	'scenario' => 'admin-create',
		    	'params' => [
		    		'statusArray' => Category::getStatusArray()
		    	]
		    ],
		    'update' => [
		    	'class' => UpdateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'update',
		    	'scenario' => 'admin-update',
		    	'params' => [
		    		'statusArray' => Category::getStatusArray()
		    	]
		    ],
		    'delete' => [
		    	'class' => DeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'batch-delete' => [
		    	'class' => BatchDeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'ordering' => [
		    	'class' => OrderingAction::className(),
		    	'model' => $this->modelClass
		    ]
		];
	}
}