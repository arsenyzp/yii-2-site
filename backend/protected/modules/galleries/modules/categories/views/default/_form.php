<?php
/**
 * @var yii\web\View $this
 * @var backend\modules\users\models\User $model
 * @var yii\widgets\ActiveForm $form
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\blogs\modules\categories\models\Category;

$form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => true,
	'validateOnChange' => false
]);
echo $form->field($model, 'title') .
     $form->field($model, 'status_id')->dropDownList($statusArray, [
     	'prompt' => 'Выберите статус'
     ]) .
     $form->field($model, 'ordering') .
     Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
     	'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
     ]);
ActiveForm::end(); ?>