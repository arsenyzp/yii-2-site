<?php

namespace backend\modules\galleries\controllers;

use backend\modules\admin\actions\crud\BatchDeleteAction;
use backend\modules\admin\actions\crud\DeleteAction;
use backend\modules\admin\actions\crud\IndexAction;
use backend\modules\admin\actions\crud\ViewAction;
use backend\modules\admin\actions\grid\OrderingAction;
use backend\modules\admin\components\Controller;
use backend\modules\galleries\models\search\GallerySearch;
use common\modules\galleries\models\Gallery;
use common\modules\galleries\models\Image;
use common\modules\galleries\modules\categories\models\Category;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\ActiveForm;
use Yii;

/**
 * Основной контроллер модуля Blogs
 */
class DefaultController extends Controller
{
    /**
     * @return string Класс основной модели контролера.
     */
    public function getModelClass()
    {
        return Gallery::className();
    }

    /**
     * @return string Класс основной поисковой модели контролера.
     */
    public function getSearchModelClass()
    {
        return GallerySearch::className();
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::className(),
                'model' => $this->modelClass,
                'searchModel' => $this->searchModelClass,
                'view' => 'index',
                'params' => [
                    'statusArray' => Gallery::getStatusArray(),
                ]
            ],
            'view' => [
                'class' => ViewAction::className(),
                'model' => $this->modelClass,
                'view' => 'view'
            ],
            'delete' => [
                'class' => DeleteAction::className(),
                'model' => $this->modelClass
            ],
            'batch-delete' => [
                'class' => BatchDeleteAction::className(),
                'model' => $this->modelClass
            ],
            'ordering' => [
                'class' => OrderingAction::className(),
                'model' => $this->modelClass
            ]
        ];
    }

    /**
     * Страница создания новой модели
     * В случае успеха, пользователь будет перенаправлен на view страницу
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery(['scenario' => 'admin-create']);
        $imageModel = new Image;
        $statusArray = Gallery::getStatusArray();
        $categoryArray = Category::getCategoryArray();

        if (Yii::$app->request->isPost) {
            $model->load($_POST);
            $models = [];
            if (($images = Yii::$app->getRequest()->post('Image')) && is_array($images)) {
                foreach ($images as $key => $value) {
                    if (isset($images[$key])) {
                        $image = new Image(['scenario' => 'admin-create']);
                        $image->setAttributes($images[$key]);
                        $models[] = $image;
                    }
                }
            }
            if ($model->validate() && Model::validateMultiple($models)) {
                $model->items = $models;
                if ($model->save(false)) {
                    return $this->redirect(['update', 'id' => $model['id']]);
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array_merge(ActiveForm::validate($model), ActiveForm::validateMultiple($models));
            }
        }

        return $this->render('create', [
            'model' => $model,
            'imageModel' => $imageModel,
            'statusArray' => $statusArray,
            'categoryArray' => $categoryArray
        ]);
    }

    /**
     * Страница обновления модели
     * В случае успеха, пользователь будет перенаправлен на view страницу
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /** @var Gallery $model */
        $model = $this->findModel($id);
        $model->setScenario('admin-update');
        $imageModel = new Image;
        $statusArray = Gallery::getStatusArray();
        $categoryArray = Category::getCategoryArray();

        if (Yii::$app->request->isPost) {
            $model->load($_POST);
            $items = $models = [];
            if (($images = Yii::$app->getRequest()->post('Image')) && is_array($images)) {
                //$model->populateRelation('images', []);
                foreach ($images as $key => $value) {
                    if (isset($value['name'])) {
                        $image = new Image(['scenario' => 'admin-create']);
                        $image->setAttributes($images[$key]);
                        $items[] = $image;
                        $models[] = $image;
                    } elseif (isset($model->images[$key])) {
                        $image = $model->images[$key];
                        $image->setScenario('admin-update');
                        $image->setAttributes($images[$key]);
                        $models[] = $image;
                    }
                }
                /*print_r($models);
                $model->populateRelation('images', $models);
                print_r( $model->images);*/
            }
            if ($model->validate() && Model::validateMultiple($models)) {
                if (!empty($items)) {
                    $model->items = $items;
                }
                if ($model->save(false)) {
                    foreach ($model->images as $item) {
                        $item->save(false);
                    }
                    //die();
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array_merge(ActiveForm::validate($model), ActiveForm::validateMultiple($models));
            }
        }

        return $this->render('update', [
            'model' => $model,
            'imageModel' => $imageModel,
            'statusArray' => $statusArray,
            'categoryArray' => $categoryArray
        ]);
    }
}
