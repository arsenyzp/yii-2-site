<?php
namespace backend\modules\blogs\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\HttpException;
use yii\filters\AccessControl;
use yii\widgets\ActiveForm;
use backend\modules\admin\components\Controller;
use backend\modules\blogs\models\search\PostSearch;
use backend\modules\admin\actions\crud\IndexAction;
use backend\modules\admin\actions\crud\ViewAction;
use backend\modules\admin\actions\crud\CreateAction;
use backend\modules\admin\actions\crud\UpdateAction;
use backend\modules\admin\actions\crud\DeleteAction;
use backend\modules\admin\actions\crud\BatchDeleteAction;
use common\modules\blogs\models\Post;
use common\modules\users\models\User;
use common\modules\blogs\modules\categories\models\Category;
use common\extensions\fileapi\actions\UploadAction as FileAPIUploadAction;
use common\extensions\fileapi\actions\DeleteAction as FileAPIDeleteAction;
use common\extensions\imperavi\actions\UploadAction as ImperaviUploadAction;
use common\extensions\imperavi\actions\GetAction as ImperaviGetAction;

/**
 * Основной контроллер backend-модуля [[Blogs]]
 */
class DefaultController extends Controller
{
	/**
	 * @return string Класс основной модели контролера.
	 */
	public function getModelClass()
	{
		return Post::className();
	}

	/**
	 * @return string Класс основной поисковой модели контролера.
	 */
	public function getSearchModelClass()
	{
		return PostSearch::className();
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['access']['rules'][] = [
		    'allow' => true,
		    'roles' => [User::ROLE_MODERATOR]
		];
		$behaviors['access']['rules'][] = [
		    'allow' => true,
		    'actions' => ['uploadTempImage', 'uploadTempPreview'],
		    'verbs' => ['POST']
		];
		$behaviors['access']['rules'][] = [
		    'allow' => true,
		    'actions' => ['deleteTempImage', 'deleteTempPreview'],
		    'verbs' => ['DELETE']
		];
		return $behaviors;
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
		    'index' => [
		    	'class' => IndexAction::className(),
		    	'model' => $this->modelClass,
		    	'searchModel' => $this->searchModelClass,
		    	'view' => 'index',
		    	'params' => [
		    		'statusArray' => Post::getStatusArray(),
		    		'categoryArray' => Category::getCategoryArray()
		    	]
		    ],
		    'view' => [
		    	'class' => ViewAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'view'
		    ],
		    'create' => [
		    	'class' => CreateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'create',
		    	'scenario' => 'admin-create',
                'seo' => true,
		    	'params' => [
		    		'statusArray' => Post::getStatusArray(),
		    		'categoryArray' => Category::getCategoryArray(),
		    		'userArray' => User::getUserArray()
		    	]
		    ],
		    'update' => [
		    	'class' => UpdateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'update',
		    	'scenario' => 'admin-update',
                'seo' => true,
		    	'params' => [
		    		'statusArray' => Post::getStatusArray(),
		    		'categoryArray' => Category::getCategoryArray(),
		    		'userArray' => User::getUserArray()
		    	]
		    ],
		    'delete' => [
		    	'class' => DeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'batch-delete' => [
		    	'class' => BatchDeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'uploadTempImage' => [
		        'class' => FileAPIUploadAction::className(),
		        'path' => $this->module->imageTempPath(),
		        'types' => $this->module->imageAllowedExtensions,
		        'minWidth' => $this->module->imageWidth
		    ],
		    'deleteTempImage' => [
		        'class' => FileAPIDeleteAction::className(),
		        'path' => $this->module->imageTempPath()
		    ],
		    'uploadTempPreview' => [
		        'class' => FileAPIUploadAction::className(),
		        'path' => $this->module->previewTempPath(),
		        'types' => $this->module->previewAllowedExtensions,
		        'minHeight' => $this->module->previewHeight,
		        'minWidth' => $this->module->previewWidth
		    ],
		    'deleteTempPreview' => [
		        'class' => FileAPIDeleteAction::className(),
		        'path' => $this->module->previewTempPath()
		    ],
		    'imperavi-upload' => [
		    	'class' => ImperaviUploadAction::className(),
		    	'path' => $this->module->contentPath(),
		    	'pathUrl' => $this->module->contentUrl()
		    ],
		    'imperavi-get' => [
		    	'class' => ImperaviGetAction::className(),
		    	'path' => $this->module->contentPath(),
		    	'pathUrl' => $this->module->contentUrl()
		    ]
		];
	}

	/**
	 * Удаляем изображение поста.
	 */
	function actionDeleteImage()
	{
		if ($id = Yii::$app->request->getDelete('id')) {
			$model = $this->findModel($id);
			$model->setScenario('delete-image');
			$model->save(false);
		} else {
			throw new HttpException(400);
		}
	}

	/**
	 * Удаляем изображение поста.
	 */
	function actionDeletePreview()
	{
		if ($id = Yii::$app->request->getDelete('id')) {
			$model = $this->findModel($id);
			$model->setScenario('delete-preview');
			$model->save(false);
		} else {
			throw new HttpException(400);
		}
	}
}
