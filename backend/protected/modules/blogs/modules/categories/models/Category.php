<?php
namespace backend\modules\blogs\modules\categories\models;

/**
 * Backend модель категорий блога
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property boolean $status_id
 */
class Category extends \common\modules\blogs\modules\categories\models\Category
{
}