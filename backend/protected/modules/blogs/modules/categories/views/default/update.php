<?php
/**
 * Представление обновления категории.
 * @var yii\base\View $this Представление
 * @var common\modules\blogs\modules\category\models\Category $model Модель
 */

use yii\helpers\Html;

$this->title = 'Обновить категорию: ' . $model['title'];
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title)
];

echo $this->render('_form', [
	'model' => $model,
	'statusArray' => $statusArray
]);