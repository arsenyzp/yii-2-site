<?php
namespace backend\modules\menu;

/**
 * Backend-модуль [[Menu]]
 * Осуществляет всю работу с страницами на backend стороне
 */
class Menu extends \common\modules\menu\Menu
{
	public $controllerNamespace = 'backend\modules\menu\controllers';
}