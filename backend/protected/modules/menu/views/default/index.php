<?php
/**
 * Страница всех постов
 * @var yii\base\View $this
 * @var backend\modules\blogs\models\Post $dataProvider
 * @var backend\modules\blogs\models\search\PostSearch $searchModel
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\SerialColumn;

$this->title = 'Меню';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'gridId' => 'menu-grid'
];


echo GridView::widget([
    'id' => 'menu-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        [
            'class' => SerialColumn::className(),
        ],
        [
            'attribute' => 'title',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a($model['title'], ['view', 'id' => $model['id']]);
            },
        ],
        'description',
        [
            'attribute' => 'status_id',
            'value' => function ($model) {
                return $model->status;
            },
            'filter' => Html::activeDropDownList($searchModel, 'status_id', $statusArray, ['class' => 'form-control', 'prompt' => 'Статус'])
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Управление'
        ]
    ]
]);