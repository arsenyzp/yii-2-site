<?php

use common\modules\seo\models\Seo;
use yii\db\Schema;

class m140627_192912_alterSeoTableAddMetaKeywords extends \yii\db\Migration
{
    public function up()
    {
        $this->addColumn(Seo::tableName(), 'meta_keywords', Schema::TYPE_TEXT . ' not null after meta_description ');
    }

    public function down()
    {
        echo "m140627_192912_alterSeoTableAddMetaKeywords cannot be reverted.\n";

        return false;
    }
}
