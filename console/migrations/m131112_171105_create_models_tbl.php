<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Миграция которая создает вспомогательную таблицу с ID моделей для их связи с СЕО модулем, или другие модули в будущем.
 */
class m131112_171105_create_models_tbl extends Migration
{
	public function up()
	{
		// Настройки MySql таблицы
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Создаём таблицу пользователей
		$this->createTable('{{%models}}', array(
			'id' => Schema::TYPE_PK,
			'model_class' => Schema::TYPE_STRING . '(255) NOT NULL',
			'model_class_id' => Schema::TYPE_INTEGER . ' NOT NULL'
		), $tableOptions);

		$this->createIndex('model_class', '{{%models}}', 'model_class');
	}

	public function down()
	{
		$this->dropTable('{{%models}}');
	}
}