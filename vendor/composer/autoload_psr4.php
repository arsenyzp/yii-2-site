<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer'),
    'yii\\jui\\' => array($vendorDir . '/yiisoft/yii2-jui'),
    'yii\\imagine\\' => array($vendorDir . '/yiisoft/yii2-imagine'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'dosamigos\\gallery\\' => array($vendorDir . '/2amigos/yii2-gallery-widget'),
    'dosamigos\\fileupload\\' => array($vendorDir . '/2amigos/yii2-file-upload-widget'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
);
