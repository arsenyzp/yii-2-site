<?php
return [
    'app' => [
	    'siteDomain' => 'http://etoservis.loc',
	    'staticsDomain' => 'http://statics.etoservis.loc',
		'adminEmail' => 'admin@my-site.com',
		'robotEmail' => 'robot@my-site.com'
	],
	'components.db' => [
		'class' => 'yii\db\Connection',
		'dsn' => 'mysql:host=127.0.0.1;dbname=etoservis',
		'username' => 'root',
		'password' => '',
		'charset' => 'utf8',
		'tablePrefix' => 'ug4k_'
	]
];