<?php
namespace common\modules\tags\models;

use common\modules\seo\behaviors\SeoBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\AutoTimestamp;
use yii\helpers\ArrayHelper;
use common\modules\tags\helpers\TagsHelper;
use common\behaviors\TransliterateBehavior;
use common\behaviors\PurifierBehavior;
use common\extensions\fileapi\behaviors\UploadBehavior;
use common\modules\users\models\User;
use common\modules\blogs\models\query\PostQuery;
use common\modules\blogs\modules\categories\models\Category;
use common\modules\comments\models\Comment;

/**
 * Class Tag
 * @package common\modules\tags\models
 * Модель тэгов.
 *
 * @property integer $id ID
 * @property integer $model_id ID моедли
 * @property integer $model_class_id ID класса модели
 * @property string $name Название тэга
 */
class Tag extends ActiveRecord
{
	/**
	 * Ключи кэша которые использует модель.
	 */
	const CACHE_TAGS_LIST_DATA = 'tagsListData';

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'purifierBehavior' => [
			    'class' => PurifierBehavior::className(),
				'textAttributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['name'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['name'],
				]
			],
            'seoBehavior' => [
                'class' => SeoBehavior::className()
            ]
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%tags}}';
	}

	/**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записи по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * Выбор записи по [[name]]
	 * @param integer $id
	 */
	public static function findByName($name)
	{
		return static::find()->where(['name' => $name])->one();
	}

	/**
	 * @return array [[DropDownList]] Массив тэгов.
	 */
	public static function getTagsArray()
	{
		$key = self::CACHE_TAGS_LIST_DATA;
		Yii::$app->getCache()->delete(self::CACHE_TAGS_LIST_DATA);
		$value = Yii::$app->getCache()->get($key);
		if ($value === false) {
			if ($models = self::find()->asArray()->all()) {
				$value = [];
				foreach ($models as $model) {
					$value[] = [
						'id' => $model['id'],
						'text' => $model['name']
					];
				}
				Yii::$app->cache->set($key, $value);
			}
		}
		return $value;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['ordering'], 'integer', 'integerOnly' => true],
			['ordering', 'default', 'value' => 0],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['name', 'ordering'],
			'admin-update' => ['name', 'ordering']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
		    'id' => 'ID',
		    'name' => 'Тэг',
		    'models' => 'Сущности с этим тэгом',
		];
	}

	/**
	 * @return \yii\db\ActiveRelation Категории поста.
	 */
	public function getModels($className = null)
	{
		if ($className === null) {
			return $this->hasMany(TagModel::className(), ['tag_id' => 'id']);
		} else {
			$modelClassId = TagsHelper::crcClassName($className);
			return $this->hasMany($className, ['id' => 'model_id'])->viaTable(TagModel::tableName(), ['tag_id' => 'id'], function($query) use ($modelClassId) {
				$query->where(['model_class_id' => $modelClassId]);
			});
		}
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert)) {
			Yii::$app->getCache()->delete(self::CACHE_TAGS_LIST_DATA);
			return true;
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			TagModel::deleteAll(['tag_id' => $this->id]);
			return true;
		} else {
			return false;
		}
	}
}
