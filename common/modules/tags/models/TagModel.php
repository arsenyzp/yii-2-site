<?php
namespace common\modules\tags\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\AutoTimestamp;
use common\behaviors\TransliterateBehavior;
use common\behaviors\PurifierBehavior;
use common\extensions\fileapi\behaviors\UploadBehavior;
use common\modules\users\models\User;
use common\modules\blogs\models\query\PostQuery;
use common\modules\blogs\modules\categories\models\Category;
use common\modules\comments\models\Comment;

/**
 * Class TagModel
 * @package common\modules\tags\models
 * Модель связи тэгов с моделями.
 *
 * @property integer $tag_id ID тэга
 * @property integer $model_id ID моедли
 * @property integer $model_class_id ID класса модели
 */
class TagModel extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%tag_model}}';
	}
}