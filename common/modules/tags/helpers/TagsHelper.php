<?php
namespace common\modules\tags\helpers;

/**
 * TagsHelper
 */
class TagsHelper
{
	public static function crcClassName($className)
	{
		return sprintf("%u", crc32($className));
	}
}