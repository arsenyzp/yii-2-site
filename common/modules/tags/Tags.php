<?php
namespace common\modules\tags;

use yii\base\Module;

/**
 * Общий модуль [[Tags]]
 */
class Tags extends Module
{
	/**
	 * @var integer Количество записей на главной странице модуля.
	 */
	public $recordsPerPage = 20;
}