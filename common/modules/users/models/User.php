<?php
namespace common\modules\users\models;

use Yii;
use yii\base\ModelEvent;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use common\modules\users\Users;
use common\extensions\fileapi\behaviors\UploadBehavior;
use common\modules\blogs\models\Post;
use common\modules\comments\models\Comment;
use common\modules\users\models\query\UserQuery;

/**
 * Class User
 * @package common\modules\users\models
 * Модель пользователя.
 *
 * @property integer $id ID
 * @property string $username Логин
 * @property string $email E-mail
 * @property string $password_hash Зашифрованый пароль
 * @property string $auth_key Ключ активации учётной записи
 * @property integer $role_id Роль
 * @property integer $status_id Статус пользователя (активен, не активен)
 * @property integer $create_time Время создания
 * @property integer $update_time Время последнего обновления
 *
 * @property string $password Пароль в чистом виде
 * @property string $repassword Повторный пароль
 */
class User extends ActiveRecord implements IdentityInterface
{
	/**
	 * Статусы записей модели.
	 * - Неактивный
	 * - Активный
	 */
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;

	/**
	 * Роли пользователей.
	 */
	const ROLE_USER = 0;
	const ROLE_ADMIN = 1;
	const ROLE_SUPERADMIN = 2;
	const ROLE_MODERATOR = 3;

	/**
	 * Ключи кэша которые использует модель.
	 */
	const CACHE_USERS_LIST_DATA = 'usersListData';

	/**
	 * Переменная используется для сбора пользовательской информации, но не сохраняется в базу.
	 * @var string $password Пароль
	 */
	public $password;

	/**
	 * Переменная используется для сбора пользовательской информации, но не сохраняется в базу.
	 * @var string $repassword Повторный пароль
	 */
	public $repassword;

	/**
	 * @var string Читабельная роль пользователя.
	 */
	protected $_role;

	/**
	 * @var string Читабельный статус пользователя.
	 */
	protected $_status;

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
		    'timestampBehavior' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['create_time', 'update_time'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
				]
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%users}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записей по [[id]]
	 * @param array $id
	 */
	public static function findIdentities($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * Выбор пользователя по [[username]]
	 * @param string $username
	 */
	public static function findByUsername($username)
	{
		return static::find()->where('username = :username', [':username' => $username])->one();
	}

	/**
	 * Выборка админа по [[username]]
	 * @param string $username
	 */
	public static function findActiveAdminByUsername($username)
	{
		return static::find()->where(['and', 'username = :username', ['or', 'role_id = ' . self::ROLE_SUPERADMIN, 'role_id = ' . self::ROLE_ADMIN, 'role_id = ' . self::ROLE_MODERATOR]], [':username' => $username])->active()->one();
	}

	/**
	 * @return integer ID пользователя.
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string Ключ авторизации пользователя.
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * @return string Читабельная роли пользователя.
	 */
	public function getRole()
	{
		if ($this->_role === null) {
			$roles = self::getRoleArray();
			$this->_role = $roles[$this->role_id];
		}
		return $this->_role;
	}

	/**
	 * @return array Массив доступных ролей пользователя.
	 */
	public static function getRoleArray()
	{
		return [
		    self::ROLE_USER => 'Обычный пользователь',
		    self::ROLE_MODERATOR => 'Модератор',
		    self::ROLE_ADMIN => 'Админ',
		    self::ROLE_SUPERADMIN => 'Супер-админ'
		];
	}

	/**
	 * @return string Читабельный статус пользователя.
	 */
	public function getStatus()
	{
		if ($this->_status === null) {
			$statuses = self::getStatusArray();
			$this->_status = $statuses[$this->status_id];
		}
		return $this->_status;
	}

	/**
	 * @return array Массив доступных ролей пользователя.
	 */
	public static function getStatusArray()
	{
		return [
		    self::STATUS_ACTIVE => 'Активен',
		    self::STATUS_INACTIVE => 'Неактивен',
		];
	}

	/**
	 * @return array [[DropDownList]] массив пользователей.
	 */
	public static function getUserArray()
	{
		$key = self::CACHE_USERS_LIST_DATA;
		$value = Yii::$app->getCache()->get($key);
		if ($value === false) {
			$value = self::find()->select(['id', 'username'])->orderBy('username ASC')->asArray()->all();
			$value = ArrayHelper::map($value, 'id', 'username');
			Yii::$app->cache->set($key, $value);
		}
		return $value;
	}

	/**
	 * Валидация ключа авторизации.
	 * @param string $authKey
	 * @return boolean
	 */
	public function validateAuthKey($authKey)
	{
		return $this->auth_key === $authKey;
	}

	/**
	 * Валидация пароля.
	 * @param string $password
	 * @return boolean
	 */
	public function validatePassword($password)
	{
		return Security::validatePassword($password, $this->password_hash);
	}

	/**
	 * Валидация старого пароля.
	 * В правилах модели метод назначен как валидатор атрибута модели.
	 * @return boolean
	 */
	public function validateOldPassword()
	{
		if (!$this->validatePassword($this->oldpassword)) {
			$this->addError('oldpassword', 'Неверный текущий пароль.');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			// Логин [[username]]
			['username', 'filter', 'filter' => 'trim', 'on' => ['admin-update', 'admin-create']],
			['username', 'required', 'on' => ['admin-update', 'admin-create']],
			['username', 'match', 'pattern' => '/^[a-zA-Z0-9_-]+$/', 'on' => ['admin-update', 'admin-create']],
			['username', 'string', 'min' => 3, 'max' => 30, 'on' => ['admin-update', 'admin-create']],
			['username', 'unique', 'on' => ['admin-update', 'admin-create']],

			// E-mail [[email]]
			['email', 'filter', 'filter' => 'trim', 'on' => ['admin-update', 'admin-create']],
			['email', 'required', 'on' => ['admin-update', 'admin-create']],
			['email', 'email', 'on' => ['admin-update', 'admin-create']],
			['email', 'string', 'max' => 100, 'on' => ['admin-update', 'admin-create']],
			['email', 'unique', 'on' => ['admin-update', 'admin-create']],

			// Пароль [[password]]
			['password', 'required', 'on' => ['admin-create']],
			['password', 'string', 'min' => 6, 'max' => 30, 'on' => ['admin-update', 'admin-create']],

			// Подтверждение пароля [[repassword]]
			['repassword', 'required', 'on' => ['admin-create']],
			['repassword', 'string', 'min' => 6, 'max' => 30, 'on' => ['admin-update', 'admin-create']],
			['repassword', 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false, 'on' => 'admin-update'],

			// Имя и Фамилия [[name]] & [[surname]]
			[['name', 'surname'], 'required', 'on' => ['admin-update', 'admin-create']],
			[['name', 'surname'], 'string', 'max' => 50, 'on' => ['admin-update', 'admin-create']],
			['name', 'match', 'pattern' => '/^[a-zа-яё]+$/iu', 'on' => ['admin-update', 'admin-create']],
			['surname', 'match', 'pattern' => '/^[a-zа-яё]+(-[a-zа-яё]+)?$/iu', 'on' => ['admin-update', 'admin-create']],

			// Роль [[role_id]]
			['role_id', 'in', 'range' => array_keys(self::getRoleArray()), 'on' => ['admin-update', 'admin-create']],

			// Статус [[status_id]]
			['status_id', 'in', 'range' => array_keys(self::getStatusArray()), 'on' => ['admin-update', 'admin-create']]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			// Backend сценарии
			'admin-update' => ['username', 'email', 'password', 'repassword', 'avatar_url', 'status_id', 'role_id'],
			'admin-create' => ['username', 'email', 'password', 'repassword', 'avatar_url', 'status_id', 'role_id']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'username' => 'Логин',
			'email' => 'E-mail',
			'password' => 'Пароль',
			'repassword' => 'Подвердите пароль',
			'oldpassword' => 'Старый пароль',
			'role_id' => 'Роль',
			'status_id' => 'Статус',
			'create_time' => 'Дата регистрации',
			'update_time' => 'Дата обновления',
		];
	}

	/**
	 * @return \yii\db\ActiveRelation Посты пользователя.
	 */
	public function getPosts()
	{
		return $this->hasMany(Post::className(), ['author_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			// Проверяем если это новая запись
			if ($this->isNewRecord) {
				// Хэшируем пароль
				if (!empty($this->password)) {
					$this->password_hash = Security::generatePasswordHash($this->password);
				}
				// Задаём статус в случае если он не указан
				if (!$this->status_id) {
					$this->status_id = self::STATUS_ACTIVE;
				}
				// Задаём роль в случае если она не указана
				if (!$this->role_id) {
					$this->role_id = self::ROLE_MODERATOR;
				}
				// Генерируем уникальный ключ
				$this->auth_key = Security::generateRandomKey();
			}
			return true;
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		if (parent::beforeDelete() && $this->id != Yii::$app->getModule('users')->superAdminId) {
			if ($this->posts) {
				foreach ($this->posts as $post) {
					$post->author_id = Yii::$app->getModule('users')->superAdminId;
					$post->save(false);
				}
			}
			return true;
		}
		return false;
	}
}