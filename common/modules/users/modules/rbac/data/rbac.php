<?php
use yii\rbac\Item;
use common\modules\users\modules\rbac\components\NotGuestRule;

$notGuest = new NotGuestRule();

return [
    'rules' => [
        $notGuest->name => serialize($notGuest)
    ],
    'items' => [
        'guest' => [
            'type' => Item::TYPE_ROLE,
            'description' => 'Гость',
            'ruleName' => null,
            'data' => null
        ],
        0 => [
            'type' => Item::TYPE_ROLE,
            'description' => 'Пользователь',
            'children' => [
                'guest'
            ],
            'ruleName' => $notGuest->name,
            'data' => null
        ],
        3 => [
            'type' => Item::TYPE_ROLE,
            'description' => 'Модератор',
            'children' => [
                0,
            ],
            'ruleName' => null,
            'data' => null
        ],
        1 => [
            'type' => Item::TYPE_ROLE,
            'description' => 'Администратор',
            'children' => [
                3
            ],
            'ruleName' => null,
            'data' => null
        ],
        2 => [
            'type' => Item::TYPE_ROLE,
            'description' => 'Супер-администратор',
            'children' => [
                1
            ],
            'ruleName' => null,
            'data' => null
        ],
    ]
];
