<?php
namespace common\modules\menu\models\query;

use yii\db\ActiveQuery;
use common\modules\menu\models\Item;

/**
 * Class MenuQuery
 * @package common\modules\menu\models\query
 * Класс кастомных запросов модели [[Menu]]
 */
class ItemQuery extends ActiveQuery
{
	/**
	 * Выбираем только опубликованые меню.
	 * @param ActiveQuery $query
	 */
	public function published()
	{
		$this->andWhere(Item::tableName() . '.status_id = :status', [':status' => Item::STATUS_PUBLISHED]);
		return $this;
	}
}