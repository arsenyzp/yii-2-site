<?php
namespace common\modules\menu\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\modules\menu\models\query\ItemQuery;

/**
 * Модуль таблицы {{%menu_items}}
 *
 * @property integer $id ID
 * @property integer $menu_id ID меню к которой привязан пунтк меню
 * @property string $title Тайтл ссылки
 * @property string $url Ссылка
 * @property string $label Название
 * @property string $description Описание
 * @property integer $ordering Порядок вывода путка меню
 * @property boolean $status_id Статус
 */
class Item extends ActiveRecord
{
	/**
	 * Статусы записей модели
	 */
	const STATUS_UNPUBLISHED = 0;
	const STATUS_PUBLISHED = 1;

	/**
	 * Название кэша пунтков меню
	 */
	const CACHE_MENU_ITEMS = 'menu_items_';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%menu_items}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new ItemQuery(get_called_class());
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'purifierBehavior' => [
				'class' => 'common\behaviors\PurifierBehavior',
				'textAttributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['url', 'label', 'title', 'description'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['url', 'label', 'title', 'description'],
				]
			]
		];
	}

	/**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записей по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * @return array Массив с статусами страниц
	 */
	public static function getStatusArray()
	{
		return [
			self::STATUS_PUBLISHED => 'Опубликованно',
		    self::STATUS_UNPUBLISHED => 'Неопубликованно'
		];
	}

	/**
	 * @return array Массив всех доступных родительских страниц
	 */
	public function getParentsArray($menuId = null)
	{
		$query = self::find()->where(['parent_id' => 0]);

		if (!$this->isNewRecord && $this->parent_id === 0) {
			$query->andWhere('id <> :id', [':id' => $this->id]);
		}
		if ($menuId !== null) {
			$query->andWhere(['menu_id' => $menuId]);
		}

		$parentsArray = ArrayHelper::map($query->all(), 'id', 'title');

		return $parentsArray;
	}

	/**
	 * @return string Читабельный статус поста
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @return array Массив пунтков меню вместе с информацией о меню, который используется для вывода в виджете [[Menu]]
	 */
	public static function getMenuItems($id)
	{
		$key = self::CACHE_MENU_ITEMS . $id;
		$value = Yii::$app->getCache()->get($key);
    $value = false;
		if ($value === false) {
			$model = Menu::find()->where(['id' => $id])->with(['items' => function ($query) {
				$query->published();
			}])->published()->one();
			$value = ['model' => $model, 'items' => []];
			if ($model !== null && $model->items) {
				foreach ($model->items as $item) {
					$title = $item['title'] ? $item['title'] : $item['label'];
					$itemArray = [
					    'url' => $item['url'],
              'title' => $item['title'],
					    'label' => $item['label'],
					    'description' => $item['description'],
					    'template' => '<a href="{url}" title="' . $title . '">{label}</a>'
					];
					if ($item->children) {
						$itemArray['options'] = ['class' => 'parent'];
            $itemTemp = array();
						foreach ($item->children as $child) {
              $itemTemp[] = [
								'url' => $child['url'],
                'ordering' => $child['ordering'],
                'title' => $child['title'],
								'label' => $child['label'],
								'description' => $child['description'],
								'template' => '<a href="{url}" title="' . $title . '">{label}</a>'
							];
						}

            uasort($itemTemp, array("self", "cmp"));
            foreach($itemTemp as $it)
              $itemArray['items'][] = $it;
					}
					$value['items'][] = $itemArray;
				}
			}
			Yii::$app->cache->set($key, $value);
		}
		return $value;
	}

  public static function cmp($a, $b)
  {
    if ($a['ordering'] == $b['ordering']) {
      return 0;
    }
    return ($a['ordering'] < $b['ordering']) ? -1 : 1;
  }

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['url', 'label', 'menu_id'], 'required'],
			[['title', 'description'], 'string', 'max' => 255],
			[['ordering', 'menu_id'], 'number', 'integerOnly' => true],
			['status_id', 'in', 'range' => array_keys(self::getStatusArray())],
			['status_id', 'default', 'value' => self::STATUS_PUBLISHED],
			['menu_id', 'validateMenu'],
			['parent_id', 'exist', 'targetAttribute' => 'id'],
			['parent_id', 'default', 'value' => 0]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			// Backend сценарии
			'admin-update' => ['url', 'label', 'title', 'description', 'menu_id', 'ordering', 'status_id', 'parent_id'],
			'admin-create' => ['url', 'label', 'title', 'description', 'menu_id', 'ordering', 'status_id', 'parent_id']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'menu_id' => 'Меню',
			'url' => 'Ссылка',
			'label' => 'Название',
			'title' => 'Тайтл ссылки',
			'description' => 'Описание',
			'ordering' => 'Порядок вывода',
			'status_id' => 'Статус',
			'parent_id' => 'Родительский пунтк'
		];
	}

	/**
	 * Валидация меню
	 * В правилах модели метод назначен как валидатор атрибута модели
	 * @return boolean
	 */
	public function validateMenu()
	{
		if (!Menu::find($this->menu_id)) {
			$this->addError('menu_id', 'Выбранное меню не существует');
		}
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getMenu()
	{
		return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getChildren()
	{
		return $this->hasMany(self::className(), ['parent_id' => 'id'])->from(self::tableName() . ' children');
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if (!$this->ordering) {
				$this->ordering = 0;
			}
			return true;
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function afterSave($insert)
	{
		Yii::$app->getCache()->delete(self::CACHE_MENU_ITEMS . $this->menu_id);
		parent::afterSave($insert);
	}
}