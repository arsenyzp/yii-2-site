<?php

namespace common\modules\pages;

use Yii;
use yii\base\Module;

/**
 * Общий-модуль [[Pages]]
 * Осуществляет всю работу с страницами на общей стороне
 */
class Pages extends Module
{
	/**
	 * @var array Массив доступных для загрузки расширений изображений.
	 */
	public $imageAllowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];

	/**
	 * @var integer Ширина изображения
	 */
	public $imageWidth = 300;

	/**
	 * @var integer Высота маленького изображения
	 */
	public $imageHeight = 165;

	/**
	 * @param string $image Имя изображения
	 * @return string Путь к папке где хранятся изображения постов.
	 */
	public function imagePath($image = null)
	{
		$path = '@root/statics/web/pages';
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @param string $image Имя изображения
	 * @return string Путь к временной папке где хранятся изображения постов или путь к конкретному изображению
	 */
	public function imageTempPath($image = null)
	{
		$path = '@root/statics/tmp/pages';
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @return string Путь к папке где хранятся контент изображения.
	 */
	public function contentPath()
	{
		$path = '@root/statics/web/content/pages';
		return Yii::getAlias($path);
	}

	/**
	 * @param string $image Имя изображения.
	 * @return string URL к папке где хранится/хранятся изображение/я.
	 */
	public function imageUrl($image = null)
	{
		$url = '/pages/';
		if ($image !== null) {
			$url .= $image;
		}
		if (isset(Yii::$app->params['staticsDomain'])) {
			$url = Yii::$app->params['staticsDomain'] . $url;
		}
		return $url;
	}

	/**
	 * @return string URL к папке где хранится/хранятся контент изображение/я.
	 */
	public function contentUrl()
	{
		$url = '/content/pages/';
		if (isset(Yii::$app->params['staticsDomain'])) {
			$url = Yii::$app->params['staticsDomain'] . $url;
		}
		return $url;
	}
}
