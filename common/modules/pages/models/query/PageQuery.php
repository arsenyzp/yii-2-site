<?php
namespace common\modules\pages\models\query;

use yii\db\ActiveQuery;
use common\modules\pages\models\Page;

/**
 * Class PageQuery
 * @package common\modules\blogs\models\query
 * Класс кастомных запросов модели [[Page]]
 */
class PageQuery extends ActiveQuery
{
	/**
	 * Выбираем только родительские страницы.
	 * @param ActiveQuery $query
	 */
	public function parents()
	{
		$this->andWhere('parent_id = :parent', [':parent' => 0]);
		return $this;
	}

	/**
	 * Выбираем только страницы которые являются услугами.
	 * @param ActiveQuery $query
	 */
	public function service()
	{
		$this->andWhere('service = :service', [':service' => Page::IS_SERVICE]);
		return $this;
	}

	/**
	 * Выбираем только информационные страницы.
	 * @param ActiveQuery $query
	 */
	public function info()
	{
		$this->andWhere('info = :info', [':info' => Page::IS_INFO]);
		return $this;
	}
}