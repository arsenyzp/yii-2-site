<?php
namespace common\modules\materials;

use Yii;
use yii\base\Module;

/**
 * Общий-модуль [[Pages]]
 * Осуществляет всю работу с страницами на общей стороне
 */
class Materials extends Module
{
	/**
	 * @var integer Ширина превьюшки.
	 */
	public $thumbWidth = 600;

	/**
	 * @var integer Высота превьюшки.
	 */
	public $thumbHeight = 450;

	/**
	 * @var array Массив доступных для загрузки расширений изображений.
	 */
	public $imageAllowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];

	/**
	 * @param string $image Имя изображения
	 * @return string Путь к папке где хранятся изображения постов.
	 */
	public function imagePath($image = null)
	{
		$path = '@root/statics/web/materials';
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @param integer $id ИД галереи к которой относится изображение
	 * @param string $image Имя изображения
	 * @return string Полный путь к папке где хранится изображение
	 */
	public function thumbPath($image = null)
	{
		$path = '@root/statics/web/materials/thumbs';
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @param string $image Имя изображения
	 * @return string Путь к временной папке где хранятся изображения постов или путь к конкретному изображению
	 */
	public function imageTempPath($image = null)
	{
		$path = '@root/statics/tmp/materials';
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @return string Путь к папке где хранятся контент изображения.
	 */
	public function contentPath()
	{
		$path = '@root/statics/web/content/materials';
		return Yii::getAlias($path);
	}

	/**
	 * @param string $image Имя изображения.
	 * @return string URL к папке где хранится/хранятся изображение/я.
	 */
	public function imageUrl($image = null)
	{
		$url = '/materials/';
		if ($image !== null) {
			$url .= $image;
		}
		if (isset(Yii::$app->params['staticsDomain'])) {
			$url = Yii::$app->params['staticsDomain'] . $url;
		}
		return $url;
	}

	/**
	 * @param integer $id ИД галереи к которой относится изображение
	 * @param string $image Имя изображения
	 * @return string URL к папке где хранится изображение
	 */
	public function thumbUrl($image = null)
	{
		$url = '/materials/thumbs/';
		if ($image !== null) {
			$url .= $image;
		}
		if (isset(Yii::$app->params['staticsDomain'])) {
			$url = Yii::$app->params['staticsDomain'] . $url;
		}
		return $url;
	}

	/**
	 * @return string URL к папке где хранится/хранятся контент изображение/я.
	 */
	public function contentUrl()
	{
		$url = '/content/materials/';
		if (isset(Yii::$app->params['staticsDomain'])) {
			$url = Yii::$app->params['staticsDomain'] . $url;
		}
		return $url;
	}
}
