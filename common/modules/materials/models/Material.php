<?php
namespace common\modules\materials\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use common\modules\tags\behaviors\TagBehavior;
use common\behaviors\PurifierBehavior;
use common\modules\users\models\User;
use common\extensions\fileapi\behaviors\UploadBehavior;
use common\modules\materials\models\query\MaterialQuery;

/**
 * Модуль таблицы {{%pages}}
 *
 * @property integer $id ID
 * @property string $parent_id ID родительской страницы
 * @property string $title Заголовок
 * @property string $alias Алиас
 * @property string $content Контент
 * @property boolean $status_id Статус
 * @property integer $create_time Дата создания
 * @property integer $update_time Дата обновления
 */
class Material extends ActiveRecord
{
	/**
	 * Статусы записей модели
	 */
	const STATUS_UNPUBLISHED = 0;
	const STATUS_PUBLISHED = 1;

	/**
	 * @var array Массив всех доступных родительских материалов
	 */
	protected $_parentsArray;

	/**
	 * @var string Изображение страницы.
	 */
	protected $_image;

	/**
	 * @var string Мини-зображение страницы.
	 */
	protected $_thumb;

	/**
	 * @var integer ID предыдущей записи.
	 */
	protected $_previousIdentity;

	/**
	 * @var integer ID предыдущей записи.
	 */
	protected $_nextIdentity;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%materials}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new MaterialQuery(get_called_class());
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		$module = Yii::$app->getModule('materials');
		$behaviors = [
			'timestampBehavior' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['create_time', 'update_time'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
				]
			],
			'uploadBehavior' => [
				'class' => UploadBehavior::className(),
				'attributes' => ['image_url'],
				'scenarios' => ['admin-create', 'admin-update'],
				'deleteScenarios' => [
				    'image_url' => 'delete-image'
				],
				'path' => [
				    'image_url' => $module->imagePath()
				],
				'tempPath' => [
				    'image_url' => $module->imageTempPath()
				],
				'thumbPath' => $module->thumbPath(),
		        'thumbWidth' => $module->thumbWidth,
		        'thumbHeight' => $module->thumbHeight
			],
			'purifierBehavior' => [
			    'class' => PurifierBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['content'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['content'],
				],
				'textAttributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['title', 'color'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['title', 'color'],
				]
			],
			'tagBehavior' => [
				'class' => TagBehavior::className()
			]
		];
		return $behaviors;
	}

	/**
	 * Выбор запись по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор запись по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * @return string|null Полный URL адрес до изображения поста.
	 */
	public function getImage()
	{
		if ($this->_image === null && $this->image_url) {
			$this->_image = Yii::$app->getModule('materials')->imageUrl($this->image_url);
		}
		return $this->_image;
	}

	/**
	 * @return string|null Полный URL адрес до изображения поста.
	 */
	public function getThumb()
	{
		if ($this->_thumb === null && $this->image_url) {
			$this->_thumb = Yii::$app->getModule('materials')->thumbUrl($this->image_url);
		}
		return $this->_thumb;
	}

	/**
	 * @return array Массив с статусами страниц
	 */
	public static function getStatusArray()
	{
		return [
		    self::STATUS_UNPUBLISHED => 'Неопубликованно',
		    self::STATUS_PUBLISHED => 'Опубликованно'
		];
	}

	/**
	 * @return array Массив всех доступных родительских материалов
	 */
	public static function getParentsArray($id = null)
	{
		$query = self::find()->parents();
		if ($id !== null) {
			$query->andWhere('id <> :id', [':id' => $id]);
		}
		$parentsArray = ArrayHelper::map($query->all(), 'id', 'title');

		return $parentsArray;
	}

	/**
	 * @return string Читабельный статус поста
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @return integer ID предыдущего материала.
	 */
	public function getPreviousIdentity()
	{
		if ($this->_previousIdentity === null) {
			$query = new Query();
			$row = $query->select('id')
						->from(self::tableName())
						->where('id < :id AND parent_id = 0', [':id' => $this->id])
						->orderBy('id DESC')
						->limit(1)
						->one();

			$this->_previousIdentity = $row['id'];
		}
		return $this->_previousIdentity;
	}

	/**
	 * @return integer ID предыдущего материала.
	 */
	public function getNextIdentity()
	{
		if ($this->_nextIdentity === null) {
			$query = new Query();
			$row = $query->select('id')
						->from(self::tableName())
						->where('id > :id AND parent_id = 0', [':id' => $this->id])
						->orderBy('id ASC')
						->limit(1)
						->one();

			$this->_nextIdentity = $row['id'];
		}
		return $this->_nextIdentity;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title'], 'required'],
			[['title', 'color'], 'string', 'max' => 100],
			['color', 'required', 'when' => function($model) { return !empty($model->parent_id); }],
			['status_id', 'in', 'range' => array_keys(self::getStatusArray())],
			['status_id', 'default', 'value' => self::STATUS_PUBLISHED],
			['parent_id', 'exist', 'targetAttribute' => 'id'],
			['ordering', 'number', 'integerOnly' => true],
			['ordering', 'default', 'value' => 0]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['parent_id', 'title', 'content', 'color', 'ordering', 'status_id', 'image_url', 'tagArray'],
			'admin-update' => ['parent_id', 'title', 'content', 'color', 'ordering', 'status_id', 'image_url', 'tagArray']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'parent_id' => 'Родителсикй материал',
			'title' => 'Заголовок',
			'image_url' => 'Изображение',
			'content' => 'Текст',
			'color' => 'Название в дополнительных изображениях',
			'status_id' => 'Статус',
			'ordering' => 'Позиция',
			'create_time' => 'Дата создания',
			'update_time' => 'Дата обновления',
			'tagArray' => 'Тэги'
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getParent()
	{
		return $this->hasOne(self::className(), ['id' => 'parent_id'])->from(self::tableName() . ' parent');
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getChildren()
	{
		return $this->hasMany(self::className(), ['parent_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			// Проверяем если это новая запись.
			if ($this->isNewRecord) {
				// Определяем родителя
				if (!$this->parent_id) {
					$this->parent_id = 0;
				}
				// Определяем статус.
				if (!$this->status_id) {
					$this->status_id = self::STATUS_PUBLISHED;
				}
			}
			return true;
		}
		return false;
	}
}
