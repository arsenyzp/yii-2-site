<?php
namespace common\modules\galleries\models;

use \Imagick;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use common\modules\galleries\models\query\GalleryQuery;
use common\modules\galleries\modules\categories\models\Category;
use common\extensions\consoleRunner\ConsoleRunner;

/**
 * Общая модель галерей
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property boolean $status_id
 * @property integer $create_time
 * @property integer $update_time
 * @property array $imagesArray
 *
 * @property Image $images
 * @property Category $category
 */
class Gallery extends ActiveRecord
{
	/**
	 * @var yii\db\ActiveRecord array Массив моделей загружаемых изображений
	 */
	protected $_items;

	/**
	 * Статусы записей модели.
	 */
	const STATUS_UNPUBLISHED = 0;
	const STATUS_PUBLISHED = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%galleries}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new GalleryQuery(get_called_class());
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['create_time', 'update_time'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
				]
			]
		];
	}

	/**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записи по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	public function setItems(array $value)
	{
		$this->_items = $value;
	}

	/**
	 * @return array Массив с статусами постов
	 */
	public static function getStatusArray()
	{
		return [
			self::STATUS_PUBLISHED => 'Опубликованно',
		    self::STATUS_UNPUBLISHED => 'Неопубликованно'
		];
	}

	/**
	 * @return string Читабельный статус поста
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'category_id', 'status_id'], 'required'],
			['title', 'match', 'pattern' => '/^[a-zа-яё0-9_ -\/:"\']+$/iu'],
			['status_id', 'in', 'range' => array_keys(self::getStatusArray())],
			['status_id', 'default', 'value' => self::STATUS_PUBLISHED],
			['category_id', 'number', 'integerOnly' => true],
			['ordering', 'integer', 'integerOnly' => true],
			['ordering', 'default', 'value' => 0]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['title', 'status_id', 'category_id', 'ordering', 'items'],
			'admin-update' => ['title', 'status_id', 'category_id', 'ordering', 'items']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
			'category_id' => 'Категория',
			'ordering' => 'Позиция',
			'status_id' => 'Статус',
			'create_time' => 'Дата создания',
			'update_time' => 'Дата обновления',
			'items' => 'Изображения'
		];
	}

	/**
	 * @return \yii\db\ActiveRelation Категория галереи
	 */
	public function getCategory()
	{
		return $this->hasOne(Category::className(), ['id' => 'category_id']);
	}

	/**
	 * @return \yii\db\ActiveRelation Изображения галереи
	 */
	public function getImages()
	{
		return $this->hasMany(Image::className(), ['gallery_id' => 'id'])->indexBy('id')->orderBy('create_time DESC');
	}

	/**
	 * @inheritdoc
	 */
	public function afterSave($insert)
	{
		$this->saveItems();
		parent::afterSave($insert);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			if ($models = Image::find()->where(['gallery_id' => $this->id])->all()) {
				foreach ($models as $model) {
					$model->delete();
				}
			}
			FileHelper::removeDirectory(Yii::$app->getModule('galleries')->path($this->id));
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Сохраняем изображения галереи
	 */
	protected function saveItems()
	{
		if ($this->_items && is_array($this->_items)) {
			$module = Yii::$app->getModule('galleries');

			foreach ($this->_items as $image) {
				if (is_file($module->tempPath($image->name))) {
					$image->gallery_id = $this->id;
					if ($image->save(false) && FileHelper::createDirectory($module->path($this->id))) {
						rename($module->tempPath($image->name), $module->path($this->id, $image->name));
					}
				}
			}
			FileHelper::removeDirectory($module->tempPath());

			$cr = new ConsoleRunner();
			$cr->run('thumbs/gallery ' . $this->id);
		}
	}
}
