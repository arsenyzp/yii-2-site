<?php
namespace common\modules\galleries\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\TimestampBehavior;
use common\behaviors\PurifierBehavior;
use common\helpers\TransliterateHelper;
use common\modules\users\models\User;
use common\modules\tags\behaviors\TagBehavior;
use common\modules\galleries\models\query\ImageQuery;
use common\modules\blogs\modules\categories\models\Category;

/**
 * Общая модель изображений галерей
 *
 * @property integer $id
 * @property integer $gallery_id
 * @property string $name
 * @property integer $ordering
 */
class Image extends ActiveRecord
{
	/**
	 * Последняя запись.
	 */
	const ISNT_LAST = 0;
	const IS_LAST = 1;

	/**
	 * @var string Полный URL до изображения.
	 */
	protected $_url;

	/**
	 * @var string Полный URL до мини-изображения.
	 */
	protected $_thumb;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%gallery_image}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new ImageQuery(get_called_class());
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['create_time', 'update_time'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
				]
			],
			'purifierBehavior' => [
			    'class' => PurifierBehavior::className(),
				'textAttributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['description', 'snippet', 'last_url'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['description', 'snippet', 'last_url'],
				]
			],
			'tagBehavior' => [
				'class' => TagBehavior::className(),
				'scenarios' => ['admin-create', 'admin-update']
			]
		];
	}

	/**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записи по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * @return Читабельный формат времени создания поста.
	 */
	public function getCreateTime()
	{
		return Yii::$app->formatter->asDate($this->create_time, 'd.m.Y');
	}

	/**
	 * @return string URL изображения
	 */
	public function getUrl()
	{
		if ($this->_url === null) {
			$this->_url = Yii::$app->getModule('galleries')->pathUrl($this->gallery_id, $this->name);
		}
		return $this->_url;
	}

	/**
	 * @return string URL мини-изображения с указаными размерами
	 */
	public function getThumb()
	{
		if ($this->_thumb === null) {
			$this->_thumb = Yii::$app->getModule('galleries')->thumbUrl($this->gallery_id, $this->name);
		}
		return $this->_thumb;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['name', 'required', 'on' => 'admin-create'],
			['name', 'match', 'pattern' => '/^[a-z0-9]+.+[a-z]+$/iu'],
			['ordering', 'integer', 'integerOnly' => true],
			['ordering', 'default', 'value' => 0],
			[['snippet', 'description', 'last_url'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['name'],
			'admin-update' => ['title', 'snippet', 'description', 'last_url', 'ordering', 'tagArray'],
			'update-last' => ''
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'gallery_id' => 'Галерея',
			'name' => 'Изображение',
			'title' => 'Тайтл',
			'snippet' => 'Краткое описание',
			'description' => 'Полное описание',
			'last' => 'Последняя работа',
			'last_url' => 'Ссылка для последней работы',
			'ordering' => 'Позиция',
			'tagArray' => 'Тэги'
		];
	}

	/**
	 * @return \yii\db\ActiveRelation Галерея.
	 */
	public function getGallery()
	{
		return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert)) {
			if ($this->scenario === 'update-last') {
				self::updateAll(['last' => self::ISNT_LAST]);
				$this->last = self::IS_LAST;
			}
			return true;
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			$module = Yii::$app->getModule('galleries');
			$image = $module->path($this->gallery_id, $this->name);
			$thumb = $module->thumbPath($this->gallery_id, $this->name);
			if (is_file($image)) {
				unlink($image);
			}
			if (is_file($thumb)) {
				unlink($thumb);
			}
			return true;
		} else {
			return false;
		}
	}
}