<?php
namespace common\modules\galleries\models\query;

use yii\db\ActiveQuery;
use common\modules\galleries\models\Image;

/**
 * Class ImageQuery
 * @package common\modules\galleries\models\query
 * Класс кастомных запросов модели [[Image]]
 */
class ImageQuery extends ActiveQuery
{
	/**
	 * Выбираем последние записи.
	 * @param ActiveQuery $query
	 */
	public function last()
	{
		$this->andWhere('last = :last', [':last' => Image::IS_LAST]);
		return $this;
	}

	/**
	 * Выбираем не последние записи.
	 * @param ActiveQuery $query
	 */
	public function notlast()
	{
		$this->andWhere('last = :last', [':last' => Image::ISNT_LAST]);
		return $this;
	}
}