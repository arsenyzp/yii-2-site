<?php
namespace common\modules\galleries\models\query;

use yii\db\ActiveQuery;
use common\modules\galleries\models\Gallery;

/**
 * Class GalleryQuery
 * Класс кастомных запросов модели [[Gallery]]
 */
class GalleryQuery extends ActiveQuery
{
	/**
	 * Выбираем только опубликованые записи.
	 * @param ActiveQuery $query
	 */
	public function published()
	{
		$this->andWhere(Gallery::tableName() . '.status_id = :status', [':status' => Gallery::STATUS_PUBLISHED]);
		return $this;
	}
}