<?php
namespace common\modules\blogs\models;

use common\modules\seo\behaviors\SeoBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\TimestampBehavior;
use common\behaviors\TransliterateBehavior;
use common\behaviors\PurifierBehavior;
use common\extensions\fileapi\behaviors\UploadBehavior;
use common\modules\tags\behaviors\TagBehavior;
use common\modules\users\models\User;
use common\modules\blogs\models\query\PostQuery;
use common\modules\blogs\modules\categories\models\Category;
use common\modules\comments\models\Comment;

/**
 * Class Post
 * @package common\modules\blogs\models
 * Модель постов.
 *
 * @property integer $id ID
 * @property string $title Заголовок
 * @property string $alias Алиас
 * @property string $snippet Анонс
 * @property string $introduction Введение
 * @property string $content Контент
 * @property string $image_url Изображение поста
 * @property string $preview_url Превью изображение поста
 * @property integer $author_id ID автора
 * @property integer $status_id Статус публикации
 * @property integer $fixed Статус закрепления поста вверху выборки
 * @property integer $views Количество просмотров
 * @property integer $create_time Время создания
 * @property integer $update_time Время обновления
 */
class Post extends ActiveRecord
{
	/**
	 * Статусы публикации записей модели.
	 */
	const STATUS_UNPUBLISHED = 0;
	const STATUS_PUBLISHED = 1;

	/**
	 * @var array Идентификаторы категории поста.
	 * Используется для получения ID категорий поста.
	 * Так же используется для определения выбраных категорий поста в момент его редактирования.
	 */
	protected $_categoryIds;

	/**
	 * @var array Текущие идентификаторы категории поста.
	 * Позволяет определить если категории поста были изменены.
	 */
	protected $_oldCategoryIds;

	/**
	 * @var string Полный URL до изображения поста.
	 */
	protected $_image;

	/**
	 * @var string Полный URL до мини-изображения поста.
	 */
	protected $_preview;

	/** @var string Дата публикации поста для JUI */
	protected $_createTimeJui;

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'timestampBehavior' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['create_time', 'update_time'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
				]
			],
			'transliterateBehavior' => [
			    'class' => TransliterateBehavior::className(),
			    'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['title' => 'alias'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['title' => 'alias']
				]
			],
			'purifierBehavior' => [
			    'class' => PurifierBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['content'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['content'],
				],
				'textAttributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['title'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['title'],
				],
				'purifierOptions' => [
				    'HTML.AllowedElements' => Yii::$app->params['allowHtmlTags']
				]
			],
			'uploadBehavior' => [
				'class' => UploadBehavior::className(),
				'attributes' => ['image_url', 'preview_url'],
				'scenarios' => ['admin-create', 'admin-update'],
				'deleteScenarios' => [
				    'image_url' => 'delete-image',
				    'preview_url' => 'delete-preview'
				],
				'path' => [
				    'image_url' => Yii::$app->getModule('blogs')->imagePath(),
				    'preview_url' => Yii::$app->getModule('blogs')->previewPath(),
				],
				'tempPath' => [
				    'image_url' => Yii::$app->getModule('blogs')->imageTempPath(),
				    'preview_url' => Yii::$app->getModule('blogs')->previewTempPath()
				]
			],
			'tagBehavior' => [
				'class' => TagBehavior::className()
			],
            'seoBehavior' => [
                'class' => SeoBehavior::className()
            ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%posts}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new PostQuery(get_called_class());
    }

    /**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записи по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * Выбор записи по [[id]] и [[alias]]
	 * @param string $username
	 */
	public static function findByIdAlias($id, $alias)
	{
		return static::find()->where(['and', 'id = :id', 'alias = :alias'], [':id' => $id, ':alias' => $alias])->one();
	}

	/**
	 * Выбор опубликованной записи по [[id]] и [[alias]]
	 * @param string $username
	 */
	public static function findPublishedByIdAlias($id, $alias)
	{
		return static::find()->where(['and', 'id = :id', 'alias = :alias'], [':id' => $id, ':alias' => $alias])->published()->one();
	}

	/**
	 * Сеттер атрибута [[_categoriesId]].
	 * @param array|null $value Для целесности логики работы с категориями поста принимается только массив.
	 * @return array|null
	 */
	public function setCategoryIds($value) {
		if (is_array($value)) {
			$this->_categoryIds = $value;
		}
	}

	/**
	 * Геттер атрибута [[_categoriesId]].
	 * @return array| Простой массив с значениями ключей категорий текущего поста, или null в случае новой записи.
	 * Используется для определения выбраных категорий в момент редактирования поста.
	 */
	public function getCategoryIds()
	{
		if (!$this->isNewRecord && $this->_categoryIds === null) {
			if ($this->categories) {
				$ids = [];
				foreach ($this->categories as $category) {
					$ids[] = $category['id'];
				}
				$this->_categoryIds = $this->_oldCategoryIds = $ids;
			}
		}
		return $this->_categoryIds;
	}

	/**
	 * @return boolean Определяем если пост фиксирован.
	 */
	public function getIsFixed()
	{
		return $this->fixed == self::IS_FIXED;
	}

	/**
	 * @return Читабельный формат времени создания поста.
	 */
	public function getCreateTime()
	{
		return Yii::$app->formatter->asDate($this->create_time, 'd.m.Y');
	}

	/**
	 * @return Читабельный формат времени создания поста для админки.
	 */
	public function getCreateTimeJui()
	{
		if ($this->_createTimeJui === null) {
			$this->_createTimeJui = Yii::$app->formatter->asDate($this->create_time, 'Y-m-d');
		}

		return $this->_createTimeJui;
	}

	/**
	 * Задаем значение создания поста.
	 */
	public function setCreateTimeJui($value)
	{
		$this->_createTimeJui = $value;
	}

	/**
	 * @return string|null Полный URL адрес до изображения поста.
	 */
	public function getImage()
	{
		if ($this->_image === null && $this->image_url) {
			$this->_image = Yii::$app->getModule('blogs')->imageUrl($this->image_url);
		}
		return $this->_image;
	}

	/**
	 * @return string|null Полный URL адрес до мини-изображения поста.
	 */
	public function getPreview()
	{
		if ($this->_preview === null && $this->preview_url) {
			$this->_preview = Yii::$app->getModule('blogs')->previewUrl($this->preview_url);
		}
		return $this->_preview;
	}

	/**
	 * @return array Массив с статусами постов.
	 */
	public static function getStatusArray()
	{
		return [
		    self::STATUS_PUBLISHED => 'Опубликованно',
		    self::STATUS_UNPUBLISHED => 'Неопубликованно'
		];
	}

	/**
	 * @return string Читабельный статус поста.
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
		    // Обязательные поля
		    [['title', 'content', 'categoryIds'], 'required'],

			// Заголовок [[title]]
			['title', 'filter', 'filter' => 'trim'],
			['title', 'string', 'max' => 100],

			['status_id', 'default', 'value' => self::STATUS_PUBLISHED],

			// Категории [[categoryIds]]
			['categoryIds', 'validateCategoryIds'],

			['createTimeJui', 'date']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['title', 'alias', 'snippet', 'content', 'image_url', 'preview_url', 'categoryIds', 'author_id', 'status_id', 'tagArray', 'createTimeJui'],
			'admin-update' => ['title', 'alias', 'snippet', 'content', 'image_url', 'preview_url', 'categoryIds', 'author_id', 'status_id', 'tagArray', 'createTimeJui'],
			'delete-image' => '',
			'delete-preview' => ''
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
		    'id' => 'ID',
		    'author_id' => 'Автор',
			'title' => 'Заголовок',
			'alias' => 'Алиас',
			'snippet' => 'Анонс',
			'introduction' => 'Введение',
			'content' => 'Текст',
			'image_url' => 'Изображение',
			'preview_url' => 'Мини-изображение',
			'fixed' => 'Зафиксирована',
			'status_id' => 'Статус',
			'views' => 'Просмотры',
			'create_time' => 'Дата создания',
			'update_time' => 'Дата обновления',
			'categoryIds' => 'Категории',
			'tagArray' => 'Тэги',
			'createTimeJui' => 'Дата публикации'
		];
	}

	/**
	 * Валидация категорий.
	 * В правилах модели метод назначен как валидатор атрибута модели.
	 * @return boolean
	 */
	public function validateCategoryIds()
	{
		if (!$this->hasErrors()) {
			$query = new Query;
			$count = $query->from(Category::tableName())
			               ->where(['id' => $this->categoryIds])
			               ->count();
			if ($count != count($this->categoryIds)) {
				$this->addError('categoryIds', 'Неправильное значение «Категории».');
			}
		}
	}

	/**
	 * @return \yii\db\ActiveRelation Автор поста.
	 */
	public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
	 * @return \yii\db\ActiveRelation Категории поста.
	 */
	public function getCategories()
	{
		return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable(PostCategory::tableName(), ['post_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert)) {
			// Проверяем если это новая запись.
			if ($this->isNewRecord) {
				// Определяем автора в случае его отсутсвия.
				if (!$this->author_id) {
					$this->author_id = Yii::$app->user->identity->id;
				}
				// Определяем статус.
				if (!$this->status_id) {
					$this->status_id = self::STATUS_PUBLISHED;
				}
			}
			// Определяем дату публикации.
			if ($this->_createTimeJui !== null) {
				$this->create_time = strtotime($this->_createTimeJui);
			}
			// Определяем снипет поста.
			$introduction = explode(Yii::$app->params['morePattern'], $this->content, 2);
			if (isset($introduction[1])) {
				$this->introduction = $introduction[0];
			}
			return true;
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function afterSave($insert)
	{
		// Сохраняем категории поста.
		if ($this->categoryIds !== $this->_oldCategoryIds) {
			PostCategory::deleteAll(['post_id' => $this->id]);
			$values = [];
			foreach ($this->categoryIds as $id) {
				$values[] = [$this->id, $id];
			}
			self::getDb()->createCommand()
			             ->batchInsert(PostCategory::tableName(), ['post_id', 'category_id'], $values)->execute();
		}
		parent::afterSave($insert);
	}
}
