<?php
namespace common\modules\blogs\modules\categories;

use Yii;
use yii\base\Module;

/**
 * Модуль [[Categories]]
 * Данный моделуь осуществляет всю работу с категориями блогов
 */
class Categories extends Module
{
}