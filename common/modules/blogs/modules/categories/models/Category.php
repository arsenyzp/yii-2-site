<?php
namespace common\modules\blogs\modules\categories\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\behaviors\TransliterateBehavior;
use common\behaviors\PurifierBehavior;
use common\modules\blogs\modules\categories\models\query\CategoryQuery;

/**
 * Class Category
 * @package common\modules\blogs\modules\categories\models
 * Модель категорий постов.
 *
 * @property integer $id ID.
 * @property string $alias Алиас.
 * @property string $title Заголовок.
 * @property integer $status_id Статус публикации.
 */
class Category extends ActiveRecord
{
	/**
	 * Статусы публикации записей модели.
	 */
	const STATUS_PUBLISHED = 1;
	const STATUS_UNPUBLISHED = 0;

	/**
	 * Ключи кэша которые использует модель.
	 */
	const CACHE_CATEGORIES_LIST_DATA = 'categoriesListData';

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'transliterateBehavior' => [
			    'class' => TransliterateBehavior::className(),
			    'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['title' => 'alias'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['title' => 'alias']
				]
			],
			'purifierBehavior' => [
			    'class' => PurifierBehavior::className(),
				'textAttributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['title'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['title'],
				]
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%posts_categories}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

	/**
	 * @return array Массив с статусами постов.
	 */
	public static function getStatusArray()
	{
		return [
		    self::STATUS_UNPUBLISHED => 'Неопубликованно',
		    self::STATUS_PUBLISHED => 'Опубликованно'
		];
	}

	/**
	 * @return string Читабельный статус поста.
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @return array [[DropDownList]] массив категорий.
	 */
	public static function getCategoryArray()
	{
		$key = self::CACHE_CATEGORIES_LIST_DATA;
		$value = Yii::$app->getCache()->get($key);
		if ($value === false || empty($value)) {
			$value = self::find()->select(['id', 'title'])->orderBy('title ASC')->published()->asArray()->all();
			$value = ArrayHelper::map($value, 'id', 'title');
			Yii::$app->cache->set($key, $value);
		}
		return $value;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
		    // Общие правила
			[['title', 'alias'], 'filter', 'filter' => 'trim'],

			// Заголовок [[title]]
			['title', 'required'],

			// Статус [[status_id]]
			['status_id', 'in', 'range' => array_keys(self::getStatusArray())],
			['status_id', 'default', 'value' => self::STATUS_PUBLISHED]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['title', 'alias', 'description', 'status_id'],
			'admin-update' => ['title', 'alias', 'description', 'status_id']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
		    'id' => 'ID',
			'title' => 'Заголовок',
			'alias' => 'Алиас',
			'status_id' => 'Статус'
		];
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert)) {
			Yii::$app->getCache()->delete(self::CACHE_CATEGORIES_LIST_DATA);
			// Проверяем если это новая запись
			if ($this->isNewRecord) {
				// Задаём статус в случае если он не указан
				if (!$this->status_id) {
					$this->status_id = self::STATUS_PUBLISHED;
				}
			}
			return true;
		}
		return false;
	}
}