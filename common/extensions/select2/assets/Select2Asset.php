<?php
namespace common\extensions\select2\assets;

use yii\web\AssetBundle;

/**
 * Менеджер ресурсов виджета [[Select2]].
 */
class Select2Asset extends AssetBundle
{
	public $sourcePath = '@common/extensions/select2/assets';
	public $css = [
		'vendor/select2-3.4.5/select2.css'
	];
	public $js = [
		'vendor/select2-3.4.5/select2.min.js'
	];
	public $depends = [
		'yii\web\JqueryAsset'
	];
}