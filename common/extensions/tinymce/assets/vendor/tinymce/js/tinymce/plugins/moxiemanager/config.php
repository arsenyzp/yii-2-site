<?php
// General
$moxieManagerConfig['general.debug'] = false;
$moxieManagerConfig['general.language'] = 'ru';

// Filesystem
// $moxieManagerConfig['filesystem.rootpath'] = 'd:/Free-lancing/LocalhostServer/OpenServer-4.9.0/domains/etoservis.loc/statics/web/content';
$moxieManagerConfig['filesystem.rootpath'] = '/home/hcwhauaj/public_html/etoservis.ru/statics/web/content/';
$moxieManagerConfig['filesystem.extensions'] = '*';

// Local filesystem
// $moxieManagerConfig['filesystem.local.wwwroot'] = 'd:/Free-lancing/LocalhostServer/OpenServer-4.9.0/domains/etoservis.loc/statics/web';
// $moxieManagerConfig['filesystem.local.urlprefix'] = "http://statics.etoservis.loc";
$moxieManagerConfig['filesystem.local.wwwroot'] = '/home/hcwhauaj/public_html/etoservis.ru/statics/web/';
$moxieManagerConfig['filesystem.local.urlprefix'] = "http://st.find-freelancer.pro";

// Upload
$moxieManagerConfig['upload.extensions'] = '*';
$moxieManagerConfig['upload.maxsize'] = '50MB';
$moxieManagerConfig['upload.overwrite'] = false;
$moxieManagerConfig['upload.autoresize'] = false;
$moxieManagerConfig['upload.autoresize_jpeg_quality'] = 100;
$moxieManagerConfig['upload.max_width'] = 9000;
$moxieManagerConfig['upload.max_height'] = 9000;
$moxieManagerConfig['upload.chunk_size'] = '10mb';
$moxieManagerConfig['upload.allow_override'] = '*';

// Authentication
$moxieManagerConfig['authenticator'] = 'SessionAuthenticator';

// SessionAuthenticator
$moxieManagerConfig['SessionAuthenticator.logged_in_key'] = 'isMoximanagerUser';
$moxieManagerConfig['SessionAuthenticator.config_prefix'] = 'moxiemanager';