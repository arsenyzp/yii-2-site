<?php
namespace common\extensions\imperavi;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * ImperaviRedactor class file.
 *
 * @property string $assetsPath
 * @property string $assetsUrl
 * @property array $plugins
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 *
 * @version 1.2.14
 *
 * @link https://github.com/yiiext/imperavi-redactor-widget
 * @link http://imperavi.com/redactor
 * @license https://github.com/yiiext/imperavi-redactor-widget/blob/master/license.md
 */
class Imperavi extends InputWidget
{
	/**
	 * @var array {@link http://imperavi.com/redactor/docs/ redactor options}.
	 */
	public $settings = [];

	/**
	 * @var array Default settings.
	 */
	private $_defaultSettings = [
		'convertDivs' => false,
		'visual' => false,
		'minHeight' => 500
	];

	/**
	 * @var string|null Selector pointing to textarea to initialize redactor for.
	 * Defaults to null meaning that textarea does not exist yet and will be
	 * rendered by this widget.
	 */
	public $selector;

	/**
	 * @var array
	 */
	public $plugins = [];

	/**
	 * Init widget.
	 */
	public function init()
	{
		parent::init();

		$language = strtolower(substr(Yii::$app->language , 0, 2));

		$this->settings = array_merge($this->_defaultSettings, $this->settings);

        if (!isset($this->settings['lang']) && $language !== 'en') {
        	$this->settings['lang'] = $language;
        }
        if (!isset($this->settings['plugins']) && !empty($this->plugins)) {
        	$this->settings['plugins'] = $this->plugins;
        }
		if ($this->selector === null) {
			$this->selector = $this->hasModel() ? '#' . Html::getInputId($this->model, $this->attribute) : '#' . $this->getId();
		}
	}

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		$this->registerClientScript();

		if ($this->hasModel()) {
			return Html::activeTextarea($this->model, $this->attribute, $this->options);
		} else {
			return Html::textarea($this->name, $this->value, $this->options);
		}
	}

	/**
	 * Registers the needed JavaScript.
	 */
	public function registerClientScript()
	{
		$view = $this->getView();

		$settings = !empty($this->settings) ? Json::encode($this->settings) : '';
		$selector = Json::encode($this->selector);

		$asset = ImperaviAsset::register($view);
		if (isset($this->settings['lang'])) {
			$asset->language = $this->settings['lang'];
		}
		if (isset($this->settings['plugins'])) {
			$asset->plugins = $this->settings['plugins'];
		}

		$view->registerJs("jQuery($selector).redactor($settings);");
	}
}