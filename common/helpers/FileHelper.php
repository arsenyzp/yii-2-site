<?php
namespace common\helpers;

/**
 * Filesystem helper
 */
class FileHelper extends \yii\helpers\FileHelper
{
	/**
	 * @param yii\web\UploadedFile|File $file путь к файлу для которого нужно определить расширение
	 * @return string расширение файла
	 */
	public function getExtension($file)
	{
		if (($extension = pathinfo($file, PATHINFO_EXTENSION)) !== '') {
			return strtolower($extension);

		}
		return null;
	}

	/**
	 * Возвращает размер картинки
	 * @param yii\web\UploadedFile|File $file путь к файлу для которого нужно определить размеры
	 * @return array ширина и высота картинки
	 */
	public function getSize($file)
	{
		if ($size = @getimagesize($file)) {
			return $size;
		}
		return null;
	}
}
