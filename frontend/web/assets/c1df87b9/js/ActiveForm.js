jQuery(document).ready(function($) {
    $(document).on('submit', formId, function(evt) {
        evt.preventDefault();
        $.ajax({
            url: $(formId).prop('action'),
            type: $(formId).prop('method'),
            data: $(formId).serialize(),
            success: function(data, textStatus, jqXHR) {
                $(formId).find('div[class*="field-"]').removeClass('has-error');
                $(formId).find('success-msg').hide();

                if (data.errors) {
                    $.each(data.errors, function(index, value) {
                        $(formId).find('.field-' + index).addClass('has-error');
                    });
                } else if (data.success) {
                    $(formId).find('.success-msg').show();
                }
            }
        });
    });
});