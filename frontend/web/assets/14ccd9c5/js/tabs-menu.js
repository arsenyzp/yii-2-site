jQuery(document).ready(function($) {
	/*** Скрипт таб меню (Tab Menu) ***/
	var menu = '.tabs-menu-widget';
	
	$(document).on('click', menu + '> ul a', function(evt) {
		evt.preventDefault();
		var currentIndex = $(this).parent().index(),
			menuId = '#' + $(this).parents(menu).prop('id'),
			tabs = $(menuId + ' div ul');

		$(this).parents('ul').find('li').removeClass('active');
		$(this).parent().addClass('active');
		tabs.hide().filter(function(index) {
			return index === currentIndex;
		}).show();
	});
});