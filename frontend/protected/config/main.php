<?php
use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

return [
	'id' => 'app-frontend',
	'name' => 'ЭтоСервис',
	'basePath' => dirname(__DIR__),
	'defaultRoute' => 'site/default/index',
	'layoutPath' => '@frontend/modules/site/views/layouts',
	'viewPath' => '@frontend/modules/site/views',
	'modules' => [
		'site' => [
			'class' => 'frontend\modules\site\Site'
		],
		'blogs' => [
			'class' => 'frontend\modules\blogs\Blogs'
		],
		'galleries' => [
		    'class' => 'frontend\modules\galleries\Galleries'
		],
		'pages' => [
		    'class' => 'frontend\modules\pages\Pages'
		],
		'slider' => [
		    'class' => 'frontend\modules\slider\Slider'
		],
		'search' => [
		    'class' => 'frontend\modules\search\Search'
		],
		'materials' => [
		    'class' => 'frontend\modules\materials\Materials'
		]
	],
	'components' => [
		'urlManager' => [
			'rules' => [
				// Модуль [[Site]]
				'<parent:^[\w\-]+>/<alias:^[\w\-]+>' => 'pages/default/view',
				'' => 'site/default/index',
				'<_a:(captcha|mail)>' => 'site/default/<_a>',

				// Модуль [[Galleries]]
				'gallery' => 'galleries/default/index',

				// Модуль [[Search]]
				'<_m:search>/<tag>' => '<_m>/default/index',

				// Модуль [[Materials]]
				'<_m:materials>' => '<_m>/default/index',
				'<_m:materials>/<id:\d+>' => '<_m>/default/view',

				// Модуль [[Blogs]]
                                '<_a:faq>' => 'blogs/default/<_a>',
                                'faq/<alias:[a-zA-Z0-9_-]+>' => 'blogs/default/faq-view',
				'<_m:blog>/<_a:subscription>' => '<_m>s/default/<_a>',
				'<_m:blog>/<alias:[a-zA-Z0-9_-]+>' => '<_m>s/default/view',
				'<_m:blog>/page/<page:\d+>' => '<_m>s/default/index',
				'<_m:blog>' => '<_m>s/default/index',

				// Модуль [[Pages]]
				'<alias:^[\w\-]+>' => 'pages/default/view',
				'<parent:^[\w\-]+>/<alias>' => 'pages/default/view'
			]
		],
		'errorHandler' => [
			'errorAction' => 'site/default/error',
		],
		'i18n' => [
			'translations' => [
				'site' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/modules/site/messages',
				]
			]
		]
	],
	'params' => isset($params['app']) ? $params['app'] : []
];
