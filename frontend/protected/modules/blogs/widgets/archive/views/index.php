<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html;

if ($models) : ?>
  <?php $cat = ($cat->alias == 'faq')?'faq':'blog'; ?>
    <div id="archive-widget" class="widget">
        <?php if ($this->context->title !== null) : ?>
            <h2><?= $this->context->title ?></h2>
        <?php endif; ?>
        <ul>
            <?php foreach ($models as $year => $posts) : ?>
                <li tabindex="1">
                    <strong><?= $year ?></strong>
                    <ul>
                        <?php foreach ($posts as $post) : ?>
                          <li><a href="/<?php echo $cat;?>/<?php echo $post['alias']; ?>"><?php echo $post['title'];?></a> </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>