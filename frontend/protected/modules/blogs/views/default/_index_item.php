<?php
/**
 * Шаблон поста на странице блогов
 * @var yii\base\View $this
 * @var common\modules\blogs\models\Post $model
 */

use yii\helpers\Html;

$title = /*(mb_strlen($model['title']) > 25) ? trim(mb_substr($model['title'], 0, 25, 'UTF-8')) . '...' : */ $model['title'];
?>
<article class="blog">
    <h3><?= Html::a($title, ['view', 'alias' => $model['alias']], ['title' => $model['title']]); ?></a></h3>

<p>
        <time pubdate="<?= $model->createTime ?>"><?= $model->createTime ?></time>
        / <span class="glyphicon glyphicon-eye-open"></span> <?= $model['views'] ?>
        <?php if ($model->tags) {
            $tags = '';
            foreach ($model->tags as $key => $tag) {
                if ($key !== 0) {
                    $tags .= ', ';
                }
                $tags .= Html::a('#' . $tag['name'], ['/search/default/index', 'tag' => $tag['name']]);
            } ?>
            / <?= $tags ?>
        <?php } ?>
    </p>
    
<?php if ($model['image_url']) {
        echo Html::a(Html::img($model->image, ['alt' => $model['title'], 'title' => $model['title']]), ['view', 'alias' => $model['alias']]);
    } ?>
    
</article>