<?php
/**
 * Представление формы модели
 * @var yii\base\View $this
 * @var frontend\modules\blogs\models\Blog $model
 * @var string/array $action Action формы
 * @var string $method Тип запроса
 */

use yii\helpers\Html;

use common\widgets\activeForm\ActiveForm;

$categories = array(
	 '1' => 'Идеи',
	 '2' => 'Ошибки'
);

$formOptions = array();
if ($method) {
    $formOptions['method'] = $method;
}
if ($action) {
    $formOptions['validationUrl'] = $action;
}
if ($resetAfterSubmit === false) {
    $formOptions['resetAfterSubmit'] = false;
}
if ($model->isNewRecord) {
    $formOptions['successCallback'] = 'function ($form, data) {
        jQuery("#newBlog").addClass("hidden");
        var $ajaxUpdate = jQuery(".ajaxUpdate");
        var $fixed = $ajaxUpdate.find(".fixed:last");
        var $items = $ajaxUpdate.find(".item");

        if ($fixed.length) {
            $fixed.after(data);
        } else {
            $ajaxUpdate.prepend(data);
        }

        console.log($items.length);

        if ($items.length == ' . $this->context->module->recordsPerPage . ') {
            $items.last().remove();
        }
    }';
} else {
    $formOptions['successCallback'] = 'function ($form, data) {
        jQuery("#newBlog").addClass("hidden");
        jQuery(".ajaxUpdate").html(data);
    }';
}
?>
<div class="row">
<div class="col-lg-12">
    <?php $form = ActiveForm::begin($formOptions);

        echo $form->field($model, 'title')->textInput().
             $form->field($model, 'content')->textarea().
             $form->field($model, 'category_id')->dropDownList($categories);
        if ($model->isNewRecord) {
            echo $form->field($model, 'disallow_comments')->checkbox();
        }
        echo Html::submitInput($model->isNewRecord ? $this->context->module->t('MOD_BLOGS_FORM_SUBMIT') : Yii::$app->getModule('site')->t('MOD_SITE_FORM_SUBMIT_UPDATE'), array(
                'class' => 'btn btn-primary pull-right',
                'data-loading-text' => Yii::$app->getModule('site')->t('MOD_SITE_FORM_SUBMIT_LOADING')
            ));
        
    ActiveForm::end(); ?>
</div>
</div>