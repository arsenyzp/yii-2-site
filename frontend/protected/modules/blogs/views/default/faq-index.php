<?php
/**
 * Страница всех блогов
 * @var yii\base\View $this
 * @var common\modules\blogs\models\Post $dataProvider
 */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Часто задаваемые вопросы об iPhone и iPad';
$this->params['pageClass'] = 'blog';
?>
<h1><?php echo Html::encode($this->title); ?></h1>

<section id="blogs">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}{pager}',
        'itemView' => '_faq_index_item'
    ]); ?>
</section>