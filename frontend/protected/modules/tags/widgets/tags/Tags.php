<?php
namespace frontend\modules\tags\widgets\tags;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\web\JsExpression;
use common\modules\tags\models\Tag;

/**
 * Виджет [[Tags]]
 * Список тэгов на главной странице.
 * @var yii\base\Widget $this Виджет
 *
 * Пример использования:
 * ~~~
 * echo Tags::widget();
 * ~~~
 */
class Tags extends Widget
{
	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$models = Tag::find()->orderBy('name ASC')->all();
		return $this->render('index', [
			'models' => $models
		]);
  	}
}