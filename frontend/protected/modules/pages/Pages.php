<?php
namespace frontend\modules\pages;

/**
 * Frontend-модуль [[Pages]]
 */
class Pages extends \common\modules\pages\Pages
{
	public $controllerNamespace = 'frontend\modules\pages\controllers';
}