<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html;

if ($models !== null) : 
	$count = count($models); ?>
	<article id="info-widget" class="widget">
		<h1>Рекомендуемое</h1>
		<ul>
			<li>§ 05. <a href="/blog/leather-iphone/" title="iPhone из кожи">iPhone из кожи — общая информация</a></li>
			<li>§ 04. <a href="/blog/leather-care/" title="Рекомендации по ухожу за кожей телефона (iPhone, Vertu, Tag-Heuer)">Рекомендации по ухожу за кожей телефона</a></li>
			<li>§ 03. <a href="/materials/" title="Материалы, используемые при создании эксклюзивного iPhone, их свойства и описания.">Карта материалов</a></li>
			<li>§ 03. <a href="/wallpapers/" title="Обои для iPhone 5 и 5s с эффектом параллакс">Обои для iPhone с эффектом параллакс</a></li>
			<li>§ 01. <a href="/work-2010-2012/" title="Работы по моддингу iPhone 4 и 4s за 2010-2012 года">Работы за 2010-2012 года</a></li>
		</ul>
	</article>
<?php endif; ?>