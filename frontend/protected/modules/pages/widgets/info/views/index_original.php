<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html;

if ($models !== null) : 
	$count = count($models); ?>
	<article id="info-widget" class="widget">
		<h1>Статьи/информация</h1>
		<ul>
			<?php foreach ($models as $model) :
				$route = ['/pages/default/view', 'alias' => $model['alias']];
				if ($model->parent_id !== 0) {
					$route['parent'] = $model->parent['alias'];
				} ?>
				<li><span>§ <?= str_pad($count, 2, 0, STR_PAD_LEFT)  ?>.</span> <?= Html::a($model['title'], $route) ?></li>
			<?php $count--;
			endforeach; ?>
		</ul>
	</article>
<?php endif; ?>