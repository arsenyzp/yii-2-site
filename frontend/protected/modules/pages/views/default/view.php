<?php
/**
 * Представление одной записи модели
 * @var yii\base\View $this
 * @var common\modules\pages\models\Page $model
 */

use frontend\modules\site\FotoramaAsset;

// скрипты галлереи
FotoramaAsset::register($this);

$this->title = $model->seo['seo_title'] ? : $model['title'];

$this->params['metaDescription'] = $model->seo['meta_description'] ? : $model['title'];
$this->params['metaKeywords'] = $model->seo['meta_keywords'] ? : $model['title'];

if ($model->seo['page_class']) {
    $this->params['pageClass'] = $model->seo['page_class'];
} else {
    $this->params['pageClass'] = 'site';
}
if ($model->seo['js']) {
    $this->params['js'] = $model->seo['js'];
}
if ($model->seo['css']) {
    $this->params['css'] = $model->seo['css'];
}
if ($model->seo['color']) {
    $this->params['color'] = $model->seo['color'];
}

echo $model['content'];
