<?php
namespace frontend\modules\materials\controllers;

use Yii;
use yii\web\HttpException;
use yii\web\Response;
use common\modules\materials\models\Material;
use frontend\modules\site\components\Controller;

/**
 * Основной контроллер модуля [[Materials]]
 */
class DefaultController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public $layout = '/main-full';

	/**
	 * Список все записей
	 */
	public function actionIndex()
	{
		$models = Material::find()->published()->parents()->orderBy('ordering ASC')->with('children')->all();

		return $this->render('index', [
			'models' => $models
		]);
	}
}
