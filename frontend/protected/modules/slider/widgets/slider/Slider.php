<?php
namespace frontend\modules\slider\widgets\slider;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\modules\slider\widgets\slider\SliderAsset;

/**
 * Виджет [[Slider]]
 * Слайдер на главной странице сайта.
 * @var yii\base\Widget $this Виджет
 *
 * Пример использования:
 * ~~~
 * echo Slider::widget();
 * ~~~
 */
class Slider extends Widget
{
	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$this->registerClientScript();

		$models = \common\modules\slider\models\Slider::find()->with('images')->published()->orderBy('ordering ASC')->all();

		return $this->render('index', [
			'models' => $models
		]);
  	}

  	/**
  	 * Регистрируем бандлы виджета.
  	 */
  	public function registerClientScript()
  	{
  		$view = $this->getView();
  		SliderAsset::register($view);
  	}
}