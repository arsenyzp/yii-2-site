jQuery(document).ready(function($) {
	var widget = '#slider-widget',
		slide = '.slide',
		content = '#content .container-fluid',
		textClass = '.text',
		activeClass = 'active',
		widgetMargin = $(widget).css('marginTop').replace('px', '')
		firstSlide = $(widget).find(slide + ':first-child'),
		firstSlideImages = shuffle(firstSlide.data('images').split(',')),
		scrollToId = '#content .content-right';

	$(widget).css('display', 'block');
	$(content).css('paddingTop', $(widget).height() + parseInt(widgetMargin));

	$(widget).find(textClass).css('bottom', '5em');
	$(firstSlide).css('backgroundImage', 'url("' + firstSlideImages[0] + '")').addClass(activeClass).find(textClass).animate({
		opacity: 1,
		bottom: '2em'
	}, 2000);

	$(window).on('resize', function() {
		$(content).css('paddingTop', $(widget).height() + parseInt(widgetMargin));
	});

	$(document).on('click', widget + ' .controls-nav', function(evt) {
		evt.preventDefault();

		if ($(this).hasClass('prev')) {
			prevSlide();
		} else if ($(this).hasClass('next')) {
			nextSlide();
		}
	});

	function nextSlide() {
		var active = $(widget).find(slide + '.' + activeClass),
			current = active.index(),
			count = $(widget).find(slide).length;

		if (current < count - 1) {
			showSlide(active, current + 1);
		} else {
			$('html, body').animate({
				scrollTop: $(scrollToId).offset().top
			}, 1000);
		}
	}

	function prevSlide() {
		var active = $(widget).find(slide + '.' + activeClass),
			current = active.index();

		if (current > 0) {
			showSlide(active, current - 1);
		}
	}

	function showSlide(active, current) {
		var searchedSlide = $(widget).find(slide).eq(current);
			images = shuffle(searchedSlide.data('images').split(','));

		active.removeClass(activeClass).slideUp().find(textClass).animate({
			opacity: 0,
			bottom: '5em'
		}, 1000);
		searchedSlide.css('backgroundImage', 'url("' + images[0] + '")').addClass(activeClass).slideDown().find(textClass).animate({
			opacity: 1,
			bottom: '2em'
		}, 2000);
	}
});

function shuffle(array) {
	var currentIndex = array.length,
		temporaryValue,
		randomIndex;

	while (0 !== currentIndex) {
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}
	return array;
}