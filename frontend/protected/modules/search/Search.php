<?php

namespace frontend\modules\search;

use yii\base\Module;

/**
 * Frontend-модуль [[Search]]
 */
class Search extends Module
{
	public $controllerNamespace = 'frontend\modules\search\controllers';
}