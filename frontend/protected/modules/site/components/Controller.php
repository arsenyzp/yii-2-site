<?php
namespace frontend\modules\site\components;

/**
 * Основной контроллер приложения
 * От данного контроллера унаследуются все остальные контроллеры фронтенда
 */
class Controller extends \yii\web\Controller
{
	/**
	 * @inheritdoc
	 */
	public $layout = '/main-fluid';
}