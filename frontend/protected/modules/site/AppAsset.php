<?php
namespace frontend\modules\site;

use yii\web\AssetBundle;

/**
 * основной пкет приложения
 */
class AppAsset extends AssetBundle
{
	public $sourcePath = '@frontend/modules/site/assets';
	public $css = [
		// 'css/main.css'
        'less/main.less'
	];
}