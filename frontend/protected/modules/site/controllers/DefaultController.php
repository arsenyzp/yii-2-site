<?php
namespace frontend\modules\site\controllers;

use Yii;
use yii\widgets\ActiveForm;
use yii\web\Response;
use frontend\modules\site\components\Controller;
use frontend\modules\site\models\ContactForm;

/**
 * Основной контроллер модуля Site
 */
class DefaultController extends Controller
{
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
			    'class' => 'yii\captcha\CaptchaAction',
			    'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			    'minLength' => 5,
			    'maxLength' => 5,
			    'height' => 29,
			    'width' => 72,
			    'offset' => 2,
			    'foreColor' => 1118481 // #111111
			]
		];
	}

	public function actionIndex()
	{
		$this->layout = '/home';
		return $this->render('index');
	}

	public function actionMail()
	{
		$model = new ContactForm();
		Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->load(Yii::$app->getRequest()->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
        	return ['success' => true];
        } elseif (Yii::$app->request->isAjax) {
        	return ['errors' => ActiveForm::validate($model)];
        }
	}
}
