<?php

namespace frontend\modules\site\covers;

class ActiveForm extends \yii\widgets\ActiveForm
{
    /**
     * @inheritdoc
     */
    public $enableClientValidation = false;

    /**
     * @inheritdoc
     */
    public $enableAjaxValidation = false;

    /**
     * @inheritdoc
     */
    public $validateOnSubmit = false;

    /**
     * @inheritdoc
     */
    public $validateOnChange = false;

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        $view = $this->getView();

        $view->registerJs('var formId = "#' . $this->options['id'] . '"', $view::POS_BEGIN);
        ActiveFormAsset::register($view);
    }
}