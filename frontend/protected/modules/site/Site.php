<?php
namespace frontend\modules\site;

use Yii;
use yii\base\Module;

/**
 * Модуль Site
 * Данный модуль является основным в приложения
 * Его можно считать как ядро фронтенда
 */
class Site extends Module
{
}