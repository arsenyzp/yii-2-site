<?php
/**
 * Head содержимое
 */

use frontend\modules\site\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JqueryAsset;

?>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode($this->title); ?></title>
    <link href="<?= Yii::$app->getRequest()->baseUrl; ?>/favicon.ico" rel="icon" type="image/x-icon"/>
    <link href="<?= Yii::$app->getRequest()->baseUrl; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<?php
if (isset($this->params['metaDescription']) && !empty($this->params['metaDescription'])) :
    $this->registerMetaTag(['name' => 'description', 'content' => Html::encode($this->params['metaDescription'])]);
endif;
if (isset($this->params['metaKeywords']) && !empty($this->params['metaKeywords'])) :
    $this->registerMetaTag(['name' => 'keywords', 'content' => Html::encode($this->params['metaKeywords'])]);
endif;
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]);
$this->head();

AppAsset::register($this);

if (isset($this->params['css'])) {
    $cssFiles = explode(',', $this->params['css']);
    foreach ($cssFiles as $css) {
        $this->registerCssFile('@web/css/' . $css, [AppAsset::className()]);
    }
}

if (isset($this->params['js'])) {
    $jsFiles = explode(',', $this->params['js']);
    foreach ($jsFiles as $js) {
        $this->registerJsFile('@web/js/' . $js, [AppAsset::className(), JqueryAsset::className()]);
    }
}
