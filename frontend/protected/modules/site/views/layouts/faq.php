<?php
/**
 * Основной шаблон приложения
 * @var $this \yii\base\View
 * @var $content string
 * @var $params array основные параметры View
 */

use frontend\modules\blogs\widgets\news\News;
use frontend\modules\blogs\widgets\archive\Archive;
use frontend\modules\blogs\widgets\subscription\Subscription;
?>

<?php $this->beginContent('@frontend/modules/site/views/layouts/layout.php'); ?>
    <!-- Content -->
    <div id="content">
        <!-- Container -->
        <div class="container-fluid">

            <!-- Content-Right -->
            <section class="content-right">
                <?= $content; ?>
            </section>
            <!--/ Content-Right -->

            <!-- Right -->
            <aside id="right">
                <?= News::widget([
                    'secondaryTitle' => 'Популярное',
                    'category' => 2,
                    'recent' => false
                ]); ?>
                <?= Archive::widget([
                    'title' => 'Хронология',
                    'category' => 2
                ]) ?>
                <!-- Social -->
                <div id="social-widget" class="widget">
                    <h2>Социальные сети</h2>
                    <ul>
                        <li><a href="#" class="twitter">Twitter</a></li>
                        <li><a href="#" class="vkontakte">Вконтакте</a></li>
                        <li><a href="#" class="instagram">Instagram</a></li>
                    </ul>
                    <p>Задавайте свои вопросы, общайтесь с нами, следите за новостями, смотрите фотографии и комментируйте.</p>
                </div>
                <!--/ Social -->
				<div class="widget">
					<h2>Последнее из Instagram</h2>
					<div class="instagram_export">
					</div
				</div>
				<!--/ Instagram pics -->
           </aside>
            <!--/ Right -->
        </div>
        <!--/ Container -->
    </div>
    <!--/ Content -->
<?php $this->endContent(); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="/js/instagram/spectragram.min.js"></script>
<script type="text/javascript" src="/js/instagram/settings.js"></script>