<?php
/**
 * Основной шаблон приложения
 * @var $this \yii\base\View
 * @var $content string
 * @var $params array основные параметры View
 */

use frontend\modules\menu\widgets\menu\Menu;

$pageClass = isset($this->params['pageClass']) ? ' class="page-' . $this->params['pageClass'] . '"' : '';
$pageId = (isset($this->params['color']) && $this->params['color'] == true) ? 'black' : 'white';
$footerContainerClass = $headerContainerClass = ($this->context->layout === '/main-static') ? 'container' : 'container-fluid';
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
<head>
    <?= $this->render('//layouts/head'); ?>
</head>
<body id="body-<?= $pageId ?>"<?= $pageClass ?>>
<?php $this->beginBody(); ?>

<!-- Supercontainer -->
<div id="supercontainer">  
<!-- Header -->
    <header id="header">
        <!-- Header-Inner -->
        <div class="header-inner">
            <!-- Container -->
            <div class="<?= $headerContainerClass ?>">
                <!-- Logo -->
                <a href="<?= Yii::$app->getHomeUrl(); ?>" class="logo">ЭтоСервис</a>
                <!--/ Logo -->
                <!-- Top Menu -->
                <?=
                Menu::widget([
                    'id' => 'topmenu',
                    'title' => false,
                    'menuId' => 1
                ]); ?>
                <!--/ Top Menu -->
            </div>
            <!--/ Container -->
        </div>
        <!--/ Header-Inner -->
    </header>
    <!--/ Header -->
    <!-- Content -->
    <?= $content; ?>
    <!--/ Content -->
    <!-- Footer -->
    <footer id="footer">
        <!-- Footer-Inner -->
        <div class="footer-inner">
            <!-- Container -->
            <div class="<?= $footerContainerClass ?>">
                <?= Menu::widget(['menuId' => 2]); ?>
                <?= Menu::widget(['menuId' => 3]); ?>
                <?= Menu::widget(['menuId' => 4]); ?>
                <?= Menu::widget(['menuId' => 5]); ?>
                <?= Menu::widget(['menuId' => 7]); ?>
                <div class="bottom">
                    <!-- Bottom Left -->
                    <div>
                        <span>&copy; &laquo;ЭтоСервис&raquo;, 2010-2014</span>

                        <p>Все авторские, исключительные и имущественные права защищены в соответствии с положениями
                            ч. 4 Гражданского кодекса Российской Федерации</p>
                        <ul>
                            <li><a href="/legal/">Условия копирования информации c сайта</a></li>
                            <li class="separator">|</li>
                            <li><a href="/privacy/">Заявление о соблюдении конфиденциальности</a></li>
                            <li class="separator">|</li>
                            <li><a href="/disclaimer/">Заявление об ограничении ответственности</a></li>
                        </ul>
                    </div>
                    <!--/ Bottom Left -->
                    <!-- Bottom Right -->
                    <div>
                        <a href="<?= Yii::$app->getHomeUrl(); ?>" class="logo">ЭтоСервис</a>
                        <a href="/feedback/" class="contacts">Наши адреса и телефоны</a>
                    </div>
                    <!--/ Bottom Right -->
                </div>
                <!--/ Bottom -->
            </div>
            <!-- Container -->
        </div>
        <!--/ Footer-Inner -->
    </footer>
    <!--/ Footer -->
</div>
<!--/ Supercontainer -->

<?php $this->endBody(); ?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter3662398 = new Ya.Metrika({id:3662398,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/3662398" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- VK -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?112"></script>
<script type="text/javascript">
    VK.init({apiId: 4353762, onlyWidgets: true});
</script>
<!-- VK -->
</body>
</html>
<?php $this->endPage(); ?>
