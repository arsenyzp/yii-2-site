<?php

/**
 * Основной шаблон приложения
 * @var $this \yii\base\View
 * @var $content string
 * @var $params array основные параметры View
 */

use frontend\modules\slider\widgets\slider\Slider;
use frontend\modules\blogs\widgets\news\News;
use frontend\modules\pages\widgets\services\Services;
use frontend\modules\pages\widgets\info\Info;
use frontend\modules\tags\widgets\tags\Tags;
use frontend\modules\menu\widgets\tabMenu\TabMenu;
?>
<?php $this->beginContent('@frontend/modules/site/views/layouts/layout.php'); ?>
	<!-- Slider -->
	<?= Slider::widget() ?>
	<!--/ Slider -->

	<!-- Content -->
	<div id="content" class="home">
		<!-- Container -->
		<div class="container-fluid">

			<!-- Content-Right -->
			<section class="content-right">
				<?= $content; ?>
			</section>
			<!--/ Content-Right -->

			<!-- Right -->
			<aside id="right">

				<!-- News -->
				<?= News::widget([
		          'title' => '<a title="Блог компании" href="/blog/">Блог</a>',
		          'secondaryTitle' => 'Новости',
		        ]); ?>
				<!--/ News -->

				<!-- Info -->
				<?= Info::widget(); ?>
				<!--/ Info -->

			</aside>
			<!--/ Right -->

			<!-- Widgets -->
			<?= Services::widget() ?>
			<?= Tags::widget() ?>
			<?= TabMenu::widget([
				'id' => 'repair-widget',
				'menuId' => 8
			]) ?>
			<?= TabMenu::widget([
				'id' => 'facilities-widget',
				'menuId' => 9
			]) ?>
			<!--/ Widgets -->

		</div>
		<!-- Container -->
	</div>
	<!--/ Content -->
<?php $this->endContent(); ?>
