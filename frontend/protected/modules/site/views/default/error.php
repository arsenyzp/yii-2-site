<?php

/**
 * Основной шаблон приложения
 * @var $this \yii\base\View
 * @var $content string
 * @var $params array основные параметры View
 */


use frontend\modules\tags\widgets\tags\Tags;
?>
<?php ?>
	

	<!-- Content -->
	<div id="content" class="home">
		<!-- Container -->
		<div class="container-fluid">

			
            <div class="sp3"><h1 class="align-center">Ошибка 404 — страница не найдена.</h1>
           <div class="sp2">
            <h2 class="align-center">С большой вероятностью, нужную информацию вы найдете в  <a href="/blog/" title="Блог «ЭтоСервис»">блоге</a> или в <a href="/gallery/" title="Галерея работ «ЭтоСервис»">галерее работ.</a></h2>
            </div>
            </div>
			<!-- Widgets -->
			<?= Tags::widget() ?>
			<!--/ Widgets -->

		</div>
		<!-- Container -->
	</div>
	<!--/ Content -->
<?php ?>
