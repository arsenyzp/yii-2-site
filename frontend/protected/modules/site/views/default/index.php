<?php
/**
 * Представление всех записей модели
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $models
 */

use yii\helpers\Html;
use frontend\modules\galleries\widgets\lastImage\LastImage;

$this->title = 'ЭтоСервис — эксклюзивный iPhone. Моддинг, тюнинг и ремонт iPhone.';
$this->params['pageClass'] = 'home';

$this->params['metaDescription'] = '«Этосервис» — моддинг iPhone из кожи, дерева и золота, производство корпоративных светящихся логотипов. Ремонт iPhone и iPad.';
$this->params['metaKeywords'] = 'Моддинг, тюнинг, iphone, 5s, 5, золото, porsche, дерево, кожа, крокодил, наппа, алькантара, Range Rover, Lamborghini, Ferrari, ремонт, замена дисплея, iPad, золотой iphone, корпус, цветной';

echo LastImage::widget();