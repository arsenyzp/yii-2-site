<?php
namespace frontend\modules\menu\widgets\menu;

use yii\base\Widget;
use frontend\modules\menu\widgets\menu\MenuAsset;
use frontend\modules\menu\models\Item;

/**
 * Виджет меню
 */
class Menu extends Widget
{
	/**
	 * @var integer ID меню которое нужно вывести
	 */
	public $menuId;

	/**
	 * @var boolean Показываем или скриываем заголовок меню
	 */
	public $title = true;

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$view = $this->getView();
		MenuAsset::register($view);

    	return $this->render('index', [
    		'menu' => Item::getMenuItems($this->menuId)
    	]);
  	}
}