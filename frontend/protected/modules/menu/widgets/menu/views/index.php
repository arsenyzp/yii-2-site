<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */

use yii\helpers\Html;
use yii\widgets\Menu;

if ($menu && $menu['items']) {
	if ($this->context->title === true && $menu['model']['title']) { ?>
		<div class="menu-widget">
		<?= Html::tag('h3', $menu['model']['title']); ?>
	<?php }
    echo Menu::widget([
    	'options' => ['id' => $this->context->id],
		'activeCssClass' => 'active',
		'activateParents' => true,
		'items' => $menu['items']
    ]);
    if ($this->context->title === true && $menu['model']['title']) { ?>
        </div>
    <?php }
} ?>