<?php
namespace frontend\modules\menu\widgets\tabMenu;

use yii\web\AssetBundle;

/**
 * основной пкет приложения
 */
class TabMenuAsset extends AssetBundle
{
	public $sourcePath = '@frontend/modules/menu/widgets/tabMenu/assets';
	public $js = [
		'js/tabs-menu.js'
	];
	public $depends = [
		'yii\web\JqueryAsset'
	];
}