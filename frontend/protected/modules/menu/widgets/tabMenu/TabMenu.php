<?php
namespace frontend\modules\menu\widgets\tabMenu;

use yii\base\Widget;
use frontend\modules\menu\models\Item;
use frontend\modules\menu\widgets\tabMenu\TabMenuAsset;

/**
 * Виджет меню с владками на главной странице.
 */
class TabMenu extends Widget
{
	/**
	 * @var integer ID меню которое нужно вывести
	 */
	public $menuId;

	/**
	 * @var boolean Показываем или скриываем заголовок меню
	 */
	public $title = true;

	public function run()
	{
		$view = $this->getView();
		TabMenuAsset::register($view);

    	return $this->render('index', [
    		'menu' => Item::getMenuItems($this->menuId)
    	]);
  	}
}