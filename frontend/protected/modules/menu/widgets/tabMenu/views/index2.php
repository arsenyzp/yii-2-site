<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */

use yii\helpers\Html;
use yii\widgets\Menu;

if ($menu && $menu['items']) {
	if ($this->context->title === true && $menu['model']['title']) { ?>
		<div id="<?= $this->context->id; ?>" class="tabs-menu-widget">
		<?= Html::tag('h1', $menu['model']['title']); ?>
	<?php } ?>
	<?php if (isset($menu['items'])) :
		$children = []; ?>
		<ul>
		<?php foreach ($menu['items'] as $key => $item) : ?>
			<?php if (isset($item['items'])) {
				$children[] = $item['items'];
			} ?>
			<li<?php if ($key === 0) { echo ' class="active"'; } ?>><a href="#"><h2><?= $item['label'] ?></h2></a></li>
		<?php endforeach; ?>
		</ul>
		<div>
			<?php if (!empty($children)) : ?>
				<?php foreach ($children as $child) : ?>
					<ul>
						<?php foreach ($child as $item) : ?>
							<li><?= Html::a($item['label'], $item['url']) ?></li>
							<?php if (!empty($item['description'])) { ?>
								<p><?= $item['description'] ?></p>
							<?php } ?>
						<?php endforeach; ?>
					</ul>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	<?php endif; ?>
    <?php if ($this->context->title === true && $menu['model']['title']) { ?>
        </div>
    <?php }
} ?>