<?php
namespace frontend\modules\galleries\controllers;

use Yii;
use common\modules\galleries\modules\categories\models\Category;
use frontend\modules\site\components\Controller;

/**
 * Основной контроллер модуля [[Blogs]]
 */
class DefaultController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public $layout = '/main-full';

	/**
	 * Страница всех моделей
	 * @return mixed
	 */
	public function actionIndex()
	{
		$models = Category::find()->with('galleries')->published()->orderBy('ordering ASC')->all();

		return $this->render('index', [
			'models' => $models,
		]);
	}
}