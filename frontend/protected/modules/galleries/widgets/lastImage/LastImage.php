<?php
namespace frontend\modules\galleries\widgets\lastImage;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\web\JsExpression;
use common\modules\galleries\models\Image;

/**
 * Виджет [[LastImage]]
 * Последняя работа галерей на главной странице.
 * @var yii\base\Widget $this Виджет
 * 
 * Пример использования:
 * ~~~
 * echo LastImage::widget();
 * ~~~
 */
class LastImage extends Widget
{
	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$model = Image::find()->last()->one();
		
		return $this->render('index', [
			'model' => $model
		]);
  	}
}